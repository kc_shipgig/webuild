const express = require('express');
const bodyParser = require('body-parser') 
const contentToServe = __dirname+'/dist/webuild-ng/'
const port = 4000;
var app = express();
app.use(bodyParser.json())
app.use(express.static(contentToServe),(req,res)=>{
   try{
        res.send(contentToServe)
   }catch(e){
        console.error('Wrong File Path')
   }
})
app.listen(port,()=>{
    console.log(`app is running on port ${port}`)
})



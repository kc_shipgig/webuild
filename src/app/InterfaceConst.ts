export interface Food {
    value: string;
    viewValue: string;
  }

  // mat menu context interface
export interface Item {
    id: number;
    name: string;
  }
  // end mat menu context
  
  
  
  
  
  export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
    ex: string;
    abc: string,
    perfstd: string,
    perfstdappdel: string,
    bomreqd: string,
    constype: string,
    serialno: string,
    superfloc: string,
    edccode: string,
    maintplant: string,
    startupdate: string,
  }
  export interface PeriodicElement1{
    first:'',
    second:'',
    third:'',
    fourth:'',
    fifth:''
  }
  
  export interface Dummy {
    id: number;
    value:string;HTTPStatus
    viewValue:string;
  }
  export interface dropdata {
    value: string;
    viewValue: string;
    id:number;
  }
  export interface PeriodicElement2 {
    ObjType: string;
    Floc: string;
    ShortText: string;
    PerfStd: string;
    SuperiorFloc: string;
    EDCCode: string;
    ConsType: string;
  
  }
  export interface PeriodicElement4 {
    tabledatashow: string;
    objtype: string;
    pmassembly: string;
    make: string;
    model: string;
    shortext: string;
    action: string;
  }
  
  export interface PeriodicElement5 {
    tabledatashow: string;
    pmassembly: string;
    component: string;
    componentdescription: string;
    olto: string;
    qty: string;
    parent: string;
    seqid: string;
    action: string;
  }
  export interface PeriodicElement6 {
    tabledatashow: string;
    pmassembly: string;
    pmassemblydescription: string;
    floc: string;
    flocdescription: string;
    action: string;
  }
  
  export const ELEMENT_DATA4: PeriodicElement4[] = [
    {tabledatashow: '',objtype: '', pmassembly: '', make: '', model: '', shortext: '', action: ''},
    {tabledatashow: '',objtype: '', pmassembly: '', make: '', model: '', shortext: '', action: ''},
    {tabledatashow: '',objtype: '', pmassembly: '', make: '', model: '', shortext: '', action: ''}
  ];
  export const ELEMENT_DATA5: PeriodicElement5[] = [
    {tabledatashow: '',pmassembly: '', component: '', componentdescription: '', olto: '', qty: '', parent: '', seqid: '', action: ''},
    {tabledatashow: '',pmassembly: '', component: '', componentdescription: '', olto: '', qty: '', parent: '', seqid: '', action: ''},
    {tabledatashow: '',pmassembly: '', component: '', componentdescription: '', olto: '', qty: '', parent: '', seqid: '', action: ''}
  ];
  export const ELEMENT_DATA6: PeriodicElement6[] = [
    {tabledatashow: '',pmassembly: '', pmassemblydescription: '', floc: '', flocdescription: '', action: ''},
    {tabledatashow: '',pmassembly: '', pmassemblydescription: '', floc: '', flocdescription: '', action: ''},
    {tabledatashow: '',pmassembly: '', pmassemblydescription: '', floc: '', flocdescription: '', action: ''}
  ];
  const ELEMENT_DATA2: PeriodicElement2[] = [
    {ObjType: '', Floc: '', ShortText: '',PerfStd: '',SuperiorFloc: '',EDCCode: '',ConsType: '',}
  ];
  
  
  export interface TreeNode {
    childCount:any,
    floc: string,
    flocLevel: string,
    isChild: boolean,
    shortText: string,
    subNode: [],
    superiorFloc: string
  }
  
  export interface DialogData {
    animal: string;
    name: string;
  }
  
  
  
  export interface Food {
    value: string;
    viewValue: string;
  }
  
  export interface TreeNode2 {

    Key:any,
    Make:any,
    Model:any,
    ObjType: string,
    PMAssembly: string,
    Parent:any,
    Plant:any,
    PrintValue: string,
    RefNo:any,
    Rev:any,
    shortText: string,
    isChild: boolean,
    subNode: []
  
    
  }
  
  export interface DialogData {
    animal: string;
    name: string;
  }
  

// mat menu context interface
export interface Item {
  id: number;
  name: string;
}
// end mat menu context


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  ex: string;
  abc: string,
  perfstd: string,
  perfstdappdel: string,
  bomreqd: string,
  constype: string,
  serialno: string,
  superfloc: string,
  edccode: string,
  maintplant: string,
  startupdate: string,
}
export interface PeriodicElement1{
  first:'',
  second:'',
  third:'',
  fourth:'',
  fifth:''
}

export interface PeriodicElement9{
  position:any,
  name:any
  weight:any,
  symbol:any,
  ex:any,
  abc:any,
  perfstd: any, perfstdappdel: any, bomreqd: '', constype: '', serialno: '',superfloc: '',edccode: '',maintplant: '',startupdate: ''
}


export const ELEMENT_DATA: PeriodicElement9[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', ex: '', abc: '', perfstd: '', perfstdappdel: '', bomreqd: '', constype: '', serialno: '',superfloc: '',edccode: '',maintplant: '',startupdate: ''}
];

export const ELEMENT_DATA1: PeriodicElement1[] = [
  {first:'',second:'',third:'',fourth:'',fifth:''}
];

//   data for flocBottom Component
export interface PeriodicElement {

    SourceID: string;
    DocumentDescription: string;
    DocType: string;
    DocRev: string;
    ObjectType:string;
  }
  export interface Food {
    value: string;
    viewValue: string;
  }
  
  export const ELEMENT_DATAs: any = [
    {SourceID: '', DocumentDescription: '', DocType: '', DocRev: '',ObjectType:''}
    
  ];
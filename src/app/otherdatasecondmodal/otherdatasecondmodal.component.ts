import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PmAssemblyComponent } from '../pm-assembly/pm-assembly.component';
export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-otherdatasecondmodal',
  templateUrl: './otherdatasecondmodal.component.html',
  styleUrls: ['./otherdatasecondmodal.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class OtherdatasecondmodalComponent implements OnInit {
plantsData = [];
  constructor( @Inject(MAT_DIALOG_DATA) public oTherData,  public dialogRef: MatDialogRef<PmAssemblyComponent>) {
    console.log(this.oTherData,'ref');
      dialogRef.disableClose = true;
      this.plantsData = this.oTherData.plant;
      (this.oTherData['PlanningPlant'] == null)? this.oTherData['PlanningPlant'] = '':'';
      (this.oTherData['RefNo'] == null)? this.oTherData['RefNo'] = '':'';
      (this.oTherData['UOM'] == null)? this.oTherData['UOM'] = '':'';
      (this.oTherData['BomAddDesc1'] == null)? this.oTherData['BomAddDesc1'] = '':'';
      (this.oTherData['BomAddDesc2'] == null)? this.oTherData['BomAddDesc2'] = '':''
  }
  onNoClick(): void {
    let obj = {};
    this.oTherData['PlanningPlant'].length == 0?obj['PlanningPlant']=null:obj['PlanningPlant'] = this.oTherData['PlanningPlant']
    this.oTherData['RefNo'].length == 0?obj['RefNo']=null:obj['RefNo'] = this.oTherData['RefNo']
    this.oTherData['UOM'].length == 0?obj['UOM']=null:obj['UOM'] = this.oTherData['UOM']
    this.oTherData['BomAddDesc1'].length ==0?obj['BomAddDesc1']=null:obj['BomAddDesc1'] = this.oTherData['BomAddDesc1']
    this.oTherData['BomAddDesc2'].length == 0 ?obj['BomAddDesc2']=null:obj['BomAddDesc2'] = this.oTherData['BomAddDesc2']
   console.log(obj,'obj============>')
    this.dialogRef.close(obj);
  }
  ngOnInit() {

  }

}

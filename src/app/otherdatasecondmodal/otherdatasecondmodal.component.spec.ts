import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherdatasecondmodalComponent } from './otherdatasecondmodal.component';

describe('OtherdatasecondmodalComponent', () => {
  let component: OtherdatasecondmodalComponent;
  let fixture: ComponentFixture<OtherdatasecondmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherdatasecondmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherdatasecondmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

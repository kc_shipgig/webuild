import { Component, ViewEncapsulation,ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent {
 viewToggler=false; 
 constructor(private cd :ChangeDetectorRef){  
   if(JSON.parse(sessionStorage.getItem("toggleState"))){
     this.viewToggler = JSON.parse(sessionStorage.getItem("toggleState"))

   }
 }
 switchViews(event){
   this.viewToggler = event;
   this.cd.markForCheck();
   console.log(event,'csadcs')
  }
}


import { Component, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

// import service
import { BasicService } from '../services/basicService';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class LoginComponent implements OnInit {
  check;
  passwordshown
  loginFg:FormGroup;
  inputType="password"
  // form variables for globally access
  user:any={userEmail:'',password:'',projectName:'',rememberPassword:''};
  userEmail;
  password;
  projectName;
  rememberPassword:boolean = false;

  constructor(private formBuilder: FormBuilder, private basicService: BasicService, private router: Router,private injector:Injector) {

    // form Handling in login page
    this.loginFg =  this.formBuilder.group({
      userEmail: new FormControl('',[Validators.required]),
      password: new FormControl('',[Validators.required]),
      projectName: new FormControl('',[Validators.required])
    })
    this.showUserDataPrefilled()
     }

  ngOnInit() {

  }
  showUserDataPrefilled(){
    if(localStorage.getItem("userEmail")){
    this.userEmail =  localStorage.getItem("userEmail")
    }
    if(localStorage.getItem("userCompanyName")){
     this.password = localStorage.getItem("userCompanyName")
    }
    if(localStorage.getItem("userProjectName") ){
      debugger
      this.projectName = localStorage.getItem("userProjectName")
    }
  }
//  Method to check the event is checked or not in login page
  isChecked(eventFired,check?){
    console.log("eventFired",eventFired.target.checked,check);
    this.rememberPassword = eventFired.target.checked;
    if(eventFired.target.checked == true){
      (this.userEmail != undefined)? localStorage.setItem("userEmail",this.userEmail):'';
      (this.password != undefined) ? localStorage.setItem("userCompanyName",this.password):'';
      (this.projectName != undefined) ?localStorage.setItem("userProjectName",this.projectName):'';
    }
    if(eventFired.target.checked == false){
      localStorage.removeItem("userEmail")
      localStorage.removeItem("userCompanyName")
      localStorage.removeItem("userProjectName")
    }
  }
  


//on login button click 
  loginUser() {
    console.log('%c 😁 Hey ya 🔥 ','font-size:32px;align:center;color:green;');
      this.user = "grant_type=password&UserName="+this.userEmail+"&Password="+this.password;
      if(this.rememberPassword == true){
        (this.userEmail != undefined)? localStorage.setItem("userEmail",this.userEmail):'';
        (this.password != undefined) ? localStorage.setItem("userCompanyName",this.password):'';
        (this.projectName != undefined) ?localStorage.setItem("userProjectName",this.projectName):'';
      }
    if(this.user){
      if(!this.userEmail){
        this.basicService.openSnackBar('Invalid userEmail','try again');
      }
      else if(!this.password){
        console.log('password',this.password);
        this.basicService.openSnackBar('Invalid password','try again');
      }
      // else if(!this.projectName){
      //   this.basicService.openSnackBar('Invalid password','try again');
      // }
      else{
        this.basicService.loginUser(this.user).subscribe((data:any)=>{
          if(data.access_token && data.refresh_token){
            // to save token in sessionStorage
            sessionStorage.setItem('access_token',data.access_token)
            sessionStorage.setItem('refresh_token',data.refresh_token)
            this.router.navigate(['home']);
            // end of saving token in sessionStorage
            
            // this.basicService.openSnackBar('Succesfully Login', 'USER LOGGED IN');
            this.injector.get(BasicService).openSnackBar('Succesfully Login', 'USER LOGGED IN');
            }
          else{
            this.basicService.openSnackBar('Invalid Credentials', 'TRY AGAIN ):');
          }
        });
      }
 
    }
    else{
     this.basicService.openSnackBar('Login Unsuccessfull','TRY AGAIN ):');
    }
  }
}

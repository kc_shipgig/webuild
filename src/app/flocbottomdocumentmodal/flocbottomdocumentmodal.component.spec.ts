import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlocbottomdocumentmodalComponent } from './flocbottomdocumentmodal.component';

describe('FlocbottomdocumentmodalComponent', () => {
  let component: FlocbottomdocumentmodalComponent;
  let fixture: ComponentFixture<FlocbottomdocumentmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlocbottomdocumentmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlocbottomdocumentmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


import { Component, OnInit, Inject, ViewEncapsulation, HostListener } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { DialogData } from '../floc-screen/floc-screen.component';
import { BasicService } from '../services/basicService';
import {FormControl} from '@angular/forms';
import {  ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
export interface PeriodicElement {
  DocID: string;
  SourceID: string;
  DocDesc: string;
  DocType: string;
  DocRev: string;
  DocType1:string;
  FlocDocs: string;
}
export interface Food {
  value: string;
  viewValue: string;
}

const ELEMENT_DATA: any = [
  {DocID: '',SourceID: '', DocDesc: '', DocType: '', DocRev: '',DocType1:'',FlocDocs:''}
];

@Component({
  selector: 'app-flocbottomdocumentmodal',
  templateUrl: './flocbottomdocumentmodal.component.html',
  styleUrls: ['./flocbottomdocumentmodal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FlocbottomdocumentmodalComponent implements OnInit {
  dataCount=0;
  stopCall = false;
  loader = false;
  selectedIndex;
  // for search in ObjType <tr>
  myControl = new FormControl();
  myControl1 = new FormControl();

  popUpTableData = new MatTableDataSource();;
  displayedColumns: string[] = ['playBuuton','SourceID','Document Description', 'DocType','DocRev',  'Object Type'];
  dataSource = ELEMENT_DATA;
  dropDownValues;
  valueMapper;
  
  constructor(
    public dialogRef: MatDialogRef<FlocbottomdocumentmodalComponent>,
    private cd: ChangeDetectorRef,@Inject(MAT_DIALOG_DATA) public data: DialogData,private basicService:BasicService,private router:Router) {
      
      // to get the data for dropDown
      this.basicService.listFlocBasicData().then((data:any)=>{
        console.log("data of pop up Table=>",data)
        this.dropDownValues = data;
      })

      // to get the data of the pop up table from the home page 
      this.basicService.formDocumentData(this.dataCount).then((data:any)=>{
        console.log("flocbottomdocument==>",data);
        if(data == undefined ){
          this.onNoClick();
          this.router.navigate(['login']);
        }
        else{
         this.handlingNullData(data).then((data)=>{
                     console.log('popUp',data);
          this.popUpTableData.data = data['result'];

          // to make readonly content to user & when user press/select the play button then its value is 'true'
          this.popUpTableData.data.map(o=>{
            o['displayKey'] = false;  
          })
          console.log('popUpTableData.data',this.popUpTableData.data);
         }).catch((error:any)=>{console.log('errorInPopUp',error)});      
        }
      });  
    }

    // method for DocType & ObjType to bind a particuar value from API
    displayFnDocType(val) {
     return  val.DocType;
    }

    displayFnObjType(val) {
     return  val.ObjType;
    }
    // end

    async handlingNullData(apiDataPassed){
      console.log('popUpapiDataPassed',apiDataPassed);
      return new Promise((resolve,reject)=>{
        // loop for removing the null error in mat form fields
        for(let i=0;i<apiDataPassed.result.length;i++){
          if(apiDataPassed.result[i].DocType == null){
            apiDataPassed.result[i].DocType = '';
          }
        }
        for(let i=0;i<apiDataPassed.result.length;i++){
            if(apiDataPassed.result[i]['ObjType'] == null){
              apiDataPassed.result[i]['ObjType'] = '';
            }

          }
          resolve(apiDataPassed);
      });          
    }

    @HostListener('scroll', ['$event']) 
    async loadMore(event){
      let pos = event.target.scrollTop + event.target.offsetHeight;
      let max = event.target.scrollHeight
      // console.log( parseInt(pos), max)
      if (parseInt(pos) == max && this.stopCall == false) {
        this.loader = true
        this.dataCount+=15;
        this.basicService.formDocumentData(this.dataCount).then((data)=>{
          if(data['result'].length > 0){
            let concatedData = [...this.popUpTableData.data, ...data['result']]
            this.popUpTableData =  new MatTableDataSource(concatedData ) ;
            this.loader = false
          }else{
            this.loader = false
            this.stopCall = true 
          }
        }).catch(e=>{
          console.error(e)
        })
            }
  }
 
  ngOnInit() {
    // to detect the mouse right click
    window.addEventListener('contextmenu',this.mouseRightClickEvent,true);

  }
  // @HostListener('document:contextmenu', ['$event']) 
  mouseRightClickEvent = (event): void =>{
    // console.log("for u eventmouse",event.target.innerText.split(' ')[1]);
    event.preventDefault(); // Suppress the browser's context menu
  }
  onNoClick(): void {
    this.dataCount = 0;
    this.loader = false;
    this.stopCall = false;
    this.dialogRef.close();
  }

  objTypeSearchOptions;
  // for search response of ObjType
  getObjType(eventCapture){
    if(eventCapture.target.value.length >= 1){
     
      let objTypePassToApi = {'ListName':'ObjType','Hint':eventCapture.target.value}
      this.basicService.searchFormDocumentDropListData(objTypePassToApi).then((response:any)=>{
        
        if( response.result != null){
          this.objTypeSearchOptions = response.result;
        }
        else if(response.message === 'Data Null' && response.statusCode === 404){
          this.objTypeSearchOptions = ["No match found"];
        }
        else{
          this.objTypeSearchOptions = ["No match found"];
        }
      })
    }
    else{
      this.objTypeSearchOptions = ['Enter atleast 1 character'];
    }

  }



  //for search response of DocType
  docTypeSearchOptions;
  getDocType(eventCapture){
    if(eventCapture.target.value.length >= 1){
     
      let objTypePassToApi = {'ListName':'DocType','Hint':eventCapture.target.value}
      this.basicService.searchFormDocumentDropListData(objTypePassToApi).then((response:any)=>{
        
        if( response.result != null){
          this.docTypeSearchOptions = response.result;
        }
        else if(response.message === 'Data Null' && response.statusCode === 404){
          this.docTypeSearchOptions = ["No match found"];
        }
        else{
          this.docTypeSearchOptions = ["No match found"];
        }
      })
    }
    else{
      this.docTypeSearchOptions = ['Enter atleast 1 character'];
    }

  }


  selectedRow={};
  playButtonclick(rowAsObjectPassed){
    this.popUpTableData.data.map(o=>{
      o['displayKey'] = false;  
    })
    this.selectedRow = rowAsObjectPassed;
    this.selectedRow['displayKey'] = true;
    console.log('rowAsObjectPassed',rowAsObjectPassed);
  }

  docPopUpRowAdd(){
    let objToAddRow={
      DocDesc: "",
      DocID: '',
      DocRev: '',
      DocType: "",
      DocTypeList: [],
      ObjType: "",
      ObjTypeList:  [],
      SourceID: "",
      key:'rowAddByUser'
    }
    if(this.popUpTableData.data != null){
      this.popUpTableData.data      = [...this.popUpTableData.data ,objToAddRow];
      this.basicService.openSnackBar('Row Added','Successfully');
    }
    else{
      this.popUpTableData.data      = [objToAddRow];  
      this.basicService.openSnackBar('Row Added','Successfully');
    }
  }

  docPopUpRowRemove(){
    if(this.popUpTableData.data[this.popUpTableData.data.length  -1]['key'] === "rowAddByUser"){
      console.log('deleteRow',this.popUpTableData.data,this.popUpTableData.data[this.popUpTableData.data.length  -1]['key']); 
      this.popUpTableData.data.pop();
      let changedArray = this.popUpTableData.data;
      this.popUpTableData.data = [...changedArray]
      this.basicService.openSnackBar('Row Deleted','Success');
    }
    else{
      this.basicService.openSnackBar('First add some row','Try again');
    }
  }

  docPopUpDeleteRow(){
    if(this.selectedRow != null && this.selectedRow['displayKey'] == true){
      if(this.selectedRow['SourceID']){
        this.basicService.flocScreenDocumentFormDataDelete(this.selectedRow['SourceID']).then((response:any)=>{
          if(response && response.statusCode === 200){
            this.basicService.openSnackBar('Successfully Deleted','Success');

            // call to get the Table Data after deleting an element
          
            this.basicService.formDocumentData(this.dataCount).then((data:any)=>{
              console.log("flocbottomdocument==>",data);
              if(data == undefined ){
                this.onNoClick();
                this.router.navigate(['login']);
              }
              else{
                this.handlingNullData(data).then((data)=>{
                console.log('popUp',data);
                this.popUpTableData.data = data['result'];

              // to make readonly content to user & when user press/select the play button then its value is 'true'
                this.popUpTableData.data.map(o=>{
                  o['displayKey'] = false;  
                })
              console.log('popUpTableData.data',this.popUpTableData.data);
              }).catch((error:any)=>{console.log('errorInPopUp',error)});      
            }
          }); 
          }
          else{
            this.basicService.openSnackBar('Not Deleted','Try Again');
          }
        })
      }
    }
    else{
      this.basicService.openSnackBar('first select some row to delete','Try Again');
    }
  }

  saveDataForDocPopUp(){
    if(this.selectedRow != null ){
      // Object.keys(this.selectedRow).map(function(k){return this.selectedRow[k]}).join(",");
      console.log('selectedRow',typeof(this.selectedRow));
      // this.selectedRow.key;
      
        this.basicService.flocScreenDocumentFormDataSave(this.selectedRow).then((response:any)=>{
        if(response && response.statusCode === 200){
          this.basicService.openSnackBar('Save Successfully','Success')
        }
        else{
          this.basicService.openSnackBar('Not Saved','Try again');
        }
        })
      }
    else{
      this.basicService.openSnackBar('Not Deleted','Try Again');
    }
  }



  //on row change data is passed
  onChangeOfRowField(rowPassed){
    console.log('rowPassed',rowPassed)
  }
}


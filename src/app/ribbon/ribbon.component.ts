import { Component, EventEmitter, Output,ChangeDetectorRef } from '@angular/core';
import{BasicService} from '../services/basicService';
import {Router} from '@angular/router';
@Component({
  selector: 'app-ribbon',
  templateUrl: './ribbon.component.html',
  styleUrls: ['./ribbon.component.css']
})
export class RibbonComponent  {
  slider
  constructor(private basicService:BasicService,private router:Router,private cd :ChangeDetectorRef){
  this.slider = sessionStorage.getItem("toggleState")
  // this.viewType.emit(Boolean(this.slider))
  // console.log("ribon")
  }
 @Output() viewType = new EventEmitter<any>();
 viewToolTip='Switch to BOM';
  showPmAssembly(event){
    console.log('state')
    if(event.checked == true){
      console.log('asdfsds')
      this.viewToolTip='Switch to Floc Screen';
      event.checked != event.checked; 
      this.viewType.emit(true)
      this.cd.markForCheck();
      sessionStorage.setItem("toggleState",'true')
    }else{
      event.checked != event.checked
      this.viewToolTip='Switch to BOM';
      this.viewType.emit(false)
      this.cd.markForCheck();
      sessionStorage.setItem("toggleState",'false')
    }   
  }

  logoutFunction() {
    this.basicService.openSnackBar('Succesfully Logout', 'USER LOGGED OUT');
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
}

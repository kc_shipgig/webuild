import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmsubassembliescomponentmodalComponent } from './pmsubassembliescomponentmodal.component';

describe('PmsubassembliescomponentmodalComponent', () => {
  let component: PmsubassembliescomponentmodalComponent;
  let fixture: ComponentFixture<PmsubassembliescomponentmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmsubassembliescomponentmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmsubassembliescomponentmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

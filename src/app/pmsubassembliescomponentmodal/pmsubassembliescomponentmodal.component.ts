import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {BasicService} from '../services/basicService'
@Component({
  selector: 'app-pmsubassembliescomponentmodal',
  templateUrl: './pmsubassembliescomponentmodal.component.html',
  styleUrls: ['./pmsubassembliescomponentmodal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PmsubassembliescomponentmodalComponent  {
  manifacturer
 subAssembly={
  Material1:'',
  MatDesc:'',
  Manufactuer:'',
  Model:'',
  UOM:'',
  IsSubAssay:0,
  RefNo:''
 }
 counter:number = 0

  constructor(  @Inject(MAT_DIALOG_DATA) public manifacturerDropdown = [],public bassicService:BasicService , public dialogRef: MatDialogRef<PmsubassembliescomponentmodalComponent>) {
    console.log(this.manifacturerDropdown,'dropDownData')
   }
    onNoClick(): void {
      this.dialogRef.close();
      this.counter=0;
    }
    saveSubAssembly(){
      this.subAssembly['Manufactuer'] = this.manifacturer ;
      console.log('log',this.subAssembly,this.manifacturer);
      return this.bassicService.addSubAssembly(this.subAssembly).then((data)=>{
        if(data && data['statusCode'] === 200){
          this.bassicService.openSnackBar('Data save successfully','success');
        }
        else if(data && data['statusCode'] === 400){
          this.bassicService.openSnackBar(data['status'],'Error');
        }
        else{
          this.bassicService.openSnackBar('Please enter a unique material','Error');
        }
        console.log(data,'cscssds')
      })
    }

    getManifacturers(event){
      this.bassicService.scrolledAtBottom(event).subscribe((atBottom)=>{
        console.log(atBottom,'cscsdcaxasxaxasx')
        if(atBottom == true){
          this.counter +=15;
          let obj = {
            "Key":"manufactuer",
            "Hint":"",
            "Limit":this.counter 
           
          }
          this.bassicService.searchBomScreenDropDownData(obj.Hint,obj.Key,obj.Limit).then((data)=>{
            console.log(data,'dataonscroll', this.manifacturerDropdown,data['result'])
            this.manifacturerDropdown = [...this.manifacturerDropdown,...data['result'].ComponentDropDownData] 
          })
        }
      })
      console.log("cscs")
    }

}

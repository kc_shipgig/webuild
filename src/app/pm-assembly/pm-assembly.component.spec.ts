import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmAssemblyComponent } from './pm-assembly.component';

describe('PmAssemblyComponent', () => {
  let component: PmAssemblyComponent;
  let fixture: ComponentFixture<PmAssemblyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmAssemblyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmAssemblyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

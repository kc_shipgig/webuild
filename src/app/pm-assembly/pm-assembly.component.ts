import { EventEmitter,Output,Component, Injectable,ViewChild,HostListener, ViewEncapsulation,DoCheck, IterableDiffers } from '@angular/core';
import { BehaviorSubject, merge, Observable, asyncScheduler } from 'rxjs';
import { map } from 'rxjs/operators';
import { BasicService } from '../services/basicService';
import {MatDialog} from '@angular/material';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { FlocbottomdocumentmodalComponent } from '../flocbottomdocumentmodal/flocbottomdocumentmodal.component';
import { MatMenuTrigger } from '@angular/material';
import { MatSelect} from '@angular/material';
import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { OtherdatafirstmodalComponent } from '../otherdatafirstmodal/otherdatafirstmodal.component';
import { OtherdatasecondmodalComponent } from '../otherdatasecondmodal/otherdatasecondmodal.component';
import { HTTPStatus } from '../services/http-interceptor';
import { PmsubassembliescomponentmodalComponent } from '../pmsubassembliescomponentmodal/pmsubassembliescomponentmodal.component';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
export interface Food {
  value: string;
  viewValue: string;
}
export interface PeriodicElement2 {
  ObjType: string;
  Floc: string;
  ShortText: string;
  PerfStd: string;
  SuperiorFloc: string;
  EDCCode: string;
  ConsType: string;
}
export interface PeriodicElement4 {
  tabledatashow: string;
  objtype: string;
  pmassembly: string;
  make: string;
  model: string;
  shortext: string;
  action: string;
}

export interface PeriodicElement5 {
  tabledatashow: string;
  pmassembly: string;
  component: string;
  componentdescription: string;
  olto: string;
  qty: string;
  parent: string;
  seqid: string;
  action: string;
}
export interface PeriodicElement6 {
  tabledatashow: string;
  pmassembly: string;
  pmassemblydescription: string;
  floc: string;
  flocdescription: string;
  action: string;
}




export interface TreeNode {
  floc: string,
  superiorFloc: string,
  shortText: string,
  flocLevel: string,
  isChild: boolean,
  subNode: [],
  childCount:any,
  item:any,

}
export interface DialogData {
  animal: string;
  name: string;
}
export interface Food {
  value: string;
  viewValue: string;
}

export interface TreeNode2 {
  item:any,
  floc: string,
  superiorFloc: string,
  shortText: string,
  flocLevel: string,
  isChild: boolean,
  subNode: [],
  childCount:any;
  
}

export interface DialogData {
  animal: string;
  name: string;
}
export class DynamicFlatNode {
  constructor(public item: TreeNode, public level = 1, public expandable = false,public isLoading = false) {
     }
}
export class DynamicDatabase {

  dataMap: TreeNode[] = [];
  rootLevelNodes: TreeNode[] = [];

  /** Initial data from database */
  initialData(): DynamicFlatNode[] {
    // console.log('treeinitialData()',this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true)));
  // console.log(this.rootLevelNodes,'dadsa')
    return this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true));
  }

  getChildren(node: TreeNode): TreeNode[] | undefined {
    return this.dataMap.filter((n: TreeNode) => {
      // console.log('nodePrint',node,n);
      return n['Parent'] === node['PrintValue']
    })
  }

  isExpandable(node: TreeNode): boolean {
    // let HasChild = this.getChildren(node);
    // return HasChild != undefined ? HasChild.length > 0 : false;
    return node.isChild;
  }
}
export class DynamicDatabase2 {

  dataMap: TreeNode[] = [];
  rootLevelNodes: TreeNode[] = [];

  /** Initial data from database */
  initialData(): DynamicFlatNode[] {
 
    return this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true));
  }

  getChildren(node: TreeNode): TreeNode[] | undefined {
    return this.dataMap.filter((n: TreeNode) => {
      // console.log('nodenode',node)
      return n['Parent'] === node['PrintValue']
    })
  }

  isExpandable(node: TreeNode): boolean {
    // let HasChild = this.getChildren(node);
    // console.log('casdcasdcascs',node.isChild)
    // return HasChild != undefined ? HasChild.length > 0 : false;
    return node.isChild;
  }
}

@Injectable()

 
export class DynamicDataSource {
  name: string;

  children: any;
  childCount:any;

  dataChange = new BehaviorSubject<DynamicFlatNode[]>([]);

  get data(): DynamicFlatNode[] { return this.dataChange.value; }
  set data(value: DynamicFlatNode[]) {
    this.treeControl.dataNodes = value;
    this.dataChange.next(value);
  }

  constructor(private treeControl: FlatTreeControl<DynamicFlatNode>,
  private database: DynamicDatabase, public apiService: BasicService) { }

  connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {
    this.treeControl.expansionModel.onChange.subscribe(change => {
      if ((change as SelectionChange<DynamicFlatNode>).added ||
        (change as SelectionChange<DynamicFlatNode>).removed) {
        this.handleTreeControl(change as SelectionChange<DynamicFlatNode>);
      }
    });

    return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
  }

  /** Handle expand/collapse behaviors */
  handleTreeControl(change: SelectionChange<DynamicFlatNode>) {
    if (change.added) {
      change.added.forEach(node => this.toggleNode(node, true));
    }
    if (change.removed) {
      change.removed.slice().reverse().forEach(node => this.toggleNode(node, false));
    }
  }

  /**
   * Toggle the node, remove from display list
   */
  toggleNode=(node: DynamicFlatNode, expand: boolean)=> {
    
    this.children = this.database.getChildren(node.item);
     node.isLoading = true;
     console.log(this.children.length, node.item.isChild, expand,node,node.item['Key'],'sdcsdsc')
    if (this.children.length == 0 && node.item.isChild && expand) {
      this.apiService.pmAssemblyTreeViewData(node.item['PrintValue'],node.item['Key']).then((data: any) => {
            node.isLoading = false;
        if (data.statusCode === 200) {
          this.children = data.result.treeData;
       //   let pmAssembly =  new PmAssemblyComponent (this.database,this.database2 ,this.basicService, this.router, this.dialog,this.httpstatus)
        //  conso,'kjjhhj')
        console.log(this.apiService,'xaxas')
         this.apiService.setToggleData(data)
      //   this.pmAssemblyTableData = data.result.AssemblyToFlocFormData
          this.addChild(node, expand);
        }
        else if(data.statusCode == 400){
          this.apiService.openSnackBar('Floc Data not found','try again');
        }
        else{

        }
      })

    }
    else {
       this.addChild(node, expand);
    }

  }

  private addChild(node: DynamicFlatNode, expand: boolean) {
    const index = this.data.indexOf(node);
    // console.log("eqweqweqweq",expand,this.children,index)
    if (!this.children || index < 0) { // If no children, or cannot find the node, no op
      return;
    }

    if (expand) {
      console.log("eqweqweqweq34",this.children)
      const nodes = this.children.map(name =>
        new DynamicFlatNode(name, node.level + 1, this.database.isExpandable(name)));
      this.data.splice(index + 1, 0, ...nodes);
    } else {  
      let count = 0;
      for (let i = index + 1; i < this.data.length
        && this.data[i].level > node.level; i++ , count++) { }
      this.data.splice(index + 1, count);
    }

    this.dataChange.next(this.data);
    node.isLoading = false;
  }

}



// mat menu context interface
export interface Item {
  id: number;
  name: string;
}
// end mat menu context





export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  ex: string;
  abc: string,
  perfstd: string,
  perfstdappdel: string,
  bomreqd: string,
  constype: string,
  serialno: string,
  superfloc: string,
  edccode: string,
  maintplant: string,
  startupdate: string,
}
export interface PeriodicElement1{
  first:'',
  second:'',
  third:'',
  fourth:'',
  fifth:''
}

export interface Dummy {
  id: number;
  value:string;HTTPStatus
  viewValue:string;
}
export interface dropdata {
  value: string;
  viewValue: string;
  id:number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', ex: '', abc: '', perfstd: '', perfstdappdel: '', bomreqd: '', constype: '', serialno: '',superfloc: '',edccode: '',maintplant: '',startupdate: ''}
];
const ELEMENT_DATA1: PeriodicElement1[] = [
  {first:'',second:'',third:'',fourth:'',fifth:''}
];
/**
 * @title Tree with dynamic data
 */

@Component({
  selector: 'app-pm-assembly',
  templateUrl: './pm-assembly.component.html',
  styleUrls: ['./pm-assembly.component.css'],
  
  encapsulation: ViewEncapsulation.None,
  providers:[DynamicDatabase
    ,DynamicDatabase2]
})
export class PmAssemblyComponent {
  highlight=false;
  RefNo;
  Rev;
  matAndSubChanges=[];
  pmAssemblyChanges = [];
  apmAssemblyChanges = [];

  serializedDate = new FormControl();
  displayedColumns4: string[] = ['tabledatashow','objtype', 'pmassembly', 'make', 'model', 'shortext', 'action'];

  displayedColumns5: string[] = ['tabledatashow','pmassembly', 'component', 'componentdescription', 'olto', 'qty', 'parent', 'seqid', 'action'];

  displayedColumns6: string[] = ['tabledatashow','pmassembly', 'pmassemblydescription', 'floc', 'flocdescription', 'action'];

  loadinController:Boolean = false 
  flocLevel:any;
  floc1:any;
  searchResultSuccess:boolean=false;
  // objType;
  searchAP;
  shortTextTable;
  perfStd;
  perfStdAppDeal;
  bomReqd;
  selectedTabIndex = 0;
  consType;
  serialNo;
  firstTableData;
  secondTableData;
  dropDownValues;
  docsRelatedData;
  iterableDiffer;

  @ViewChild('tree') tree;

   constructor(public database: DynamicDatabase,private database2 :DynamicDatabase2,private basicService: BasicService, private router: Router, public dialog: MatDialog,public httpstatus:HTTPStatus ,private _iterableDiffers: IterableDiffers) {
    // let a = document.getElementById('container')
    // console.log(a,'xsxs')
    this.basicService.toggleNode.subscribe((data)=>{
      console.log(data,'dasdasdsd')
    })
    this.httpstatus.getHttpStatus().subscribe((data)=>{
      this.loadinController = data
    })
    this.initialCall();
  }
  expanded = false;
  styleFunction(node,treeControl){
    if(node.item.PrintValue=='Project'){
      return 'custom-title'
    }
    if(treeControl.isExpanded(node) && node.item.PrintValue !== 'Project' && node.item.Key=='ObjType'){
      return 'obj-click'
    }
  }
  showCheckboxes(fromSelect,index?) {
    console.log('ADD',fromSelect,index);
    
    if(fromSelect != undefined && index != undefined  ){
 var checkboxes = document.getElementById(`checkboxes${index}`);
    console.log(fromSelect,';dsasd')
      if (!this.expanded) {
         checkboxes.style.display = "block";
        this.expanded = true;
      } else {
        checkboxes.style.display = "none";
       this.expanded = false;
      }
 
  }
  }
  calculateMarginTop(checkbox,index){
    console.log(checkbox.style.marginTop , index,checkbox,'indexssss')
    // return index * 10
    return index * 10
  }
  @Output() public sendData: EventEmitter<{}> = new EventEmitter();
  otherdatafirstmodal(dataToSend): void {
    dataToSend['plant']= this.plantsData;
    console.log(this.plantsData,'plantData')
    const dialogRef = this.dialog.open(OtherdatafirstmodalComponent, {
      data:dataToSend,
      width: '300px',
    panelClass: 'otherdatafirstmodal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result,dataToSend,'The dialog was closed');
      dataToSend['Plant'] = result.Plant;
      dataToSend['RefNo'] = result.RefNo;
      dataToSend['Rev'] = result.Rev;
     
    });
  }
  // selection(event){
  //   console.log(event,"ssssssssssssssssssssssssssssss")
  // }
  openpmsubassembliescomponenetdialog(): void {

    const dialogRef = this.dialog.open(PmsubassembliescomponentmodalComponent, {
      data:this.manifacturerData,
      panelClass: 'openpmsubassembliescomponenetdialog'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      

    });
  }
  otherdatasecondmodal(index): void {
    console.log("data on POP up click",this.matAndSubAssemblyData)
    this.matAndSubAssemblyData[index]['plant']= this.plantsData;
    const dialogRef = this.dialog.open(OtherdatasecondmodalComponent, {
      width: '400px',
      data:this.matAndSubAssemblyData[index],
    panelClass: 'otherdatasecondmodal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result,'===========================?>>????')
      this.matAndSubAssemblyData[index].PlanningPlant = result.PlanningPlant;
      this.matAndSubAssemblyData[index].RefNo = result.RefNo;
      this.matAndSubAssemblyData[index].UOM = result.UOM;
      this.matAndSubAssemblyData[index].BomAddDesc1 = result.BomAddDesc1;
      this.matAndSubAssemblyData[index].BomAddDesc2 = result.BomAddDesc2;
    });
  }
  objTypeData = [];
  plantsData = [];
  manifacturerData = [];
  

  async getPMAssemblySelectorData(){
   await this.basicService.getBomScreenSelectorData().then((data)=>{
      console.log('selector data',data)
      if(data['statusCode'] == 200){
        this.objTypeData = data['result'].objTypes;
        this.plantsData = data['result'].plants;
        this.manifacturerData = data['result'].manufactuer
      }
    })
  }
  componentToDelete:string;
  async getMatAndSubAssemblyData(component){
    this.componentToDelete = component;
    await this.basicService.getSubAssemblyAndMatDAta(component).then((data)=>{
      if(data['statusCode'] === 200){
        this.matAndSubAssemblyData = data['result'].SubAssemblyFormData;
        this.pmAssemblyTableData = data['result'].pmAssembly;
        console.log(this.pmAssemblyTableData,'dhasjdbkadsa2')
       // this.dropDown.pmAssembly.find( o=>o.PMAssembly1 == 
        this.aPMData = data['result'].AssemblyToFlocFormData;
        this.expandTree(data);

      }
      console.log(data,'MatData')
    })
  }


  associatePmAssemblyDelete;
  getParticularAssosiatePM(floc,pmAssembly){
    console.log('floc',floc,'pmAssembly',pmAssembly);
    let obj = {
      'floc':floc,
      'pmAssembly':pmAssembly
    }
    this.associatePmAssemblyDelete = obj;
    this.basicService.getParticularAssociatePmAssemblyData(obj).then((data)=>{
      if(data && data['statusCode'] == 200){
        this.matAndSubAssemblyData = data['result']['SubAssemblyFormData'] 
        this.aPMData = data['result']['AssemblyToFlocFormData']
        this.pmAssemblyTableData = data['result']['pmAssembly']
        this.dropDown.floc = data['result'].flocDropDownData
    
        console.log(this.dropDown.floc,'flooooc')
        this.validatePMAssembly();
        console.log( this.matAndSubAssemblyData, 
        this.aPMData ,
        this.pmAssemblyTableData,
        this.dropDown.floc)
        this.expandTree(data)
      }
    })
  }

  dataSource2: DynamicDataSource;
  materialSubAssemblyData=[];
  AssociatePmAssembly = [];
  initialCall(){
      if(sessionStorage.getItem('access_token') && sessionStorage.getItem('refresh_token') && sessionStorage.getItem('refresh_token')!=undefined && sessionStorage.getItem('access_token')!=undefined){
      this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
      this.dataSource = new DynamicDataSource(this.treeControl, this.database, this.basicService);
      this.dataSource.data = this.database.initialData();
      console.log('dataSource',this.dataSource);
      
      // api call for the dropdown content
      this.getPMAssemblySelectorData(); 
      this.basicService.pmAssemblyTreeViewData('').then((data:any)=>{
        if(data && data.message =='Success'){
          this.getTreeData();
        }
        this.dropDownValues=data;
        });
        this.basicService.getAssosiateAssembly(0).then((data)=>{
          console.log(data,'loging data')
          if(data['statusCode'] == 200 && Array.isArray(data['result']['PMAssemblyToFlocFormData'])){
            this.AssociatePmAssembly =data['result']['PMAssemblyToFlocFormData'];
            console.log(this.AssociatePmAssembly,'pmAssembly')
          }
        })

    // this.basicService.getMaterialAndSubAssemblyData(0).subscribe(data=>{
    //  if(data['statusCode'] == 200 && data['result'] &&data['result'].MaterialAndSubAssemblyData){
    //    this.materialSubAssemblyData = data['result'].MaterialAndSubAssemblyData;
    //    console.log(data,'cscsd')
    //  }
    // }) 
    this.dataSource2 = new DynamicDataSource(this.treeControl, this.database, this.basicService);
    this.dataSource2.data = this.database.initialData();

    this.getPmAssemblyDropDownData();
}
else{
    this.router.navigate(['login']);
    }
  }
  currentIndex
  mouseEnter(i){
    console.log("innnnnnnnnnn",i);
    this.currentIndex = i
  }



  dropDown;
  getPmAssemblyDropDownData(){
    this.basicService.getPmAssemblyDropDownData().then((dataRes:any)=>{
      this.dropDown = dataRes.result;

      // console.log('dataRes',dataRes.result.objTypes,dataRes.result.plants);
      // this.dropDown;
    })

  }


  countForRightClick:number=0;
  // @HostListener('document:contextmenu', ['$event']) 
  mouseRightClickEvent = (event): void =>{
    console.log("eventmouse",event,event.target.className);
    this.countForRightClick++;
    if(this.countForRightClick%2==0){
    }
    else if(event.target.screenX < 499){
      // this.mouseHover(event.target.innerText.split(' ')[1],this);
      this.contextMenu.closeMenu();
      event.preventDefault(); // Suppress the browser's context menu   
    }
    else{
      console.log('right click on right split area');
    }
  }

  @HostListener('document:scroll', ['$event']) 
  scroll = (event): void => {
    console.log("cscs")
    if(this.objSearchedData.flocHint.length > 3){  
      let pos = event.target.scrollTop + event.target.offsetHeight;
      let max = event.target.scrollHeight
      console.log("scrollll",pos,max);
          if (parseInt(pos) == max) {
            this.count+=50;
            this.objSearchedData.startLimit=this.count;
            this.basicService.searchFloc(this.objSearchedData).then((data:any)=>
            { 
              if( data.result != null && data.statusCode != 400){
                  this.dataForSearchTable.push(...data.result);
              }
              else{
                  this.basicService.openSnackBar('Error in Response','Backend Error');
                  }
            })
        }
        else{
            console.log("scroll is not at bottom");    
          }
    }
    else{
      // console.log("serach text length is less than 3 letter");
      
    }
}
  pmAsemblySearchCounter = 0;
  pmSearchData;
  getSearchDataOnClick(event,searchedContent){
    if(searchedContent.length>3){
   let obj = {
     'Key':'PMAssemblyToFloc',
     'Hint':searchedContent,
    'Limit':this.pmAsemblySearchCounter,   
    }
    this.basicService.getPmAssemblySearchData(obj).then((data)=>{
      this.pmSearchData = data['result'];
      console.log(this.pmSearchData,'logging data')
    });
  }
  }
  @HostListener('window:scroll', ['$event']) 
  public scrolled($event: Event) {
console.log(event,'logging')
}
  loadMaterialData(event){
    console.log('calledasasds',event)
    this.basicService.scrolledAtBottom(event).subscribe((data)=>{
      console.log(data,'logging')
    })
  }

  saveData(){
    console.log("saved")
    // this.iterableDiffer.diff()
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(FlocbottomdocumentmodalComponent, {
      data: {},
      panelClass: 'floc-bottom-table-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  description;
  descriptionBottom;
  // for table data
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'ex', 'abc', 'perfstd', 'perfstdappdel', 'bomreqd', 'constype', 'serialno', 'superfloc', 'edccode', 'maintplant', 'startupdate'];
  dataSource1 = ELEMENT_DATA;

  // table 2
  displayedColumns1: string[] = ['first', 'second', 'third', 'fourth', 'fifth'];
  myDataArray = ELEMENT_DATA1;

  maintplant: dropdata[] = [
    {id:1, value: '', viewValue: 'AU04' },
    { id:2,value: '', viewValue: 'AU05' }
  ];
 
  
  objTypeArray = [
    {id:0,value:'BOXJ'},
    {id:1,value:'VASO'},
    {id:2,value:'SWLI'},
    {id:3,value:'DESA'}
  ];
  perfStdArray=[
    {id:0,value:'F27'},
    {id:1,value:'P28'},
    {id:2,value:'F28'},
    {id:3,value:'P29'}
  ]
  // end of table data
  
  


  // mat menu for the context menu 
  myControl = new FormControl();
  
  options: string[] = [];

  items = [
    {id: 1, name: 'Item 1'},
    {id: 2, name: 'Item 2'},
    {id: 3, name: 'Item 3'}
  ];

  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  onContextMenu(event: MouseEvent, item: Item,flocPassed) {
    console.log("func u",flocPassed,"<floc passed ====event>",event);
    // this.mouseHover(flocPassed,this);
    event.preventDefault(); // Suppress the browser's context menu
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
  
  }
  searchMaterial
  materialSearchCount = 0
  stopMaterial =false;
  
  dataForMaterialSearch = [];
  showMaterialSearchOptions=false;
  matSearchOptions;
  serachingMaterialMethod(){  
    
    console.log(this.searchMaterial,'cascsds')
    if(this.searchMaterial.length == 0){
      this.materialSearchCount = 0;
      this.stopMaterial = false;
      this.dataForMaterialSearch=[];
    }
    
    if(this.searchMaterial.length>3){
      this.dataForMaterialSearch = [];
    this.objSearchedData.Hint = this.searchMaterial;
      this.showMaterialSearchOptions=false;
      this.objSearchedData.Limit = this.materialSearchCount;
      this.objSearchedData['Key'] = 'MaterialsAndSubAssembly';
      this.basicService.getPmAssemblySearchData(this.objSearchedData).then((data:any)=>
      { 
        if(data.result &&  data.result.length > 0 && data.statusCode != 400){
          this.dataForMaterialSearch.push(...data.result);
          console.log(this.dataForMaterialSearch,this.dataForMaterialSearch.length,'searchresults=====>',this.matSearchOptions)
          this.showMaterialSearchOptions = true;
          
        }
        else if(data.result == null && data.statusCode === 400 ){
         
          if(this.materialSearchCount == 0){
            this.matSearchOptions = ["No Similar Data Searched"];
            // this.showMaterialSearchOptions=true;
          }
          if(this.materialSearchCount > 0){
            this.stopMaterial = true  
            this.materialSearchCount = 0
          }
        }
        else{
          this.materialSearchCount = 0
          this.matSearchOptions = ["Error in response"];
        }
      })
    }
  }
  getMaterialSearchData(event){
    this.basicService.scrolledAtBottom(event).subscribe((getMoreData)=>{
      if(getMoreData){
        this.materialSearchCount+=5;
        if(!this.stopMaterial){
          this.serachingMaterialMethod() 
        }
      }else{
        console.log('not at Bottom')
      }
    })

  }
  // for searching a Floc at right top panel
  searchFloc;
  flocSearched=[];
  previousSearchedFlocName;//to get the value next time when we come on search
  count=0;
  objSearchedData:any={'flocHint':'','startLimit':''};
  flocSearchedShow:boolean = false;
  matOption;
  dataForSearchTable=[];
  stopPM = false
  serachingPMMethod(){    
    if(this.searchFloc.length == 0){
      this.count = 0;
      this.stopPM = false;
      this.dataForSearchTable=[];
    }
    if(this.searchFloc.length>3){
    this.flocSearched = [];
    this.objSearchedData.Hint = this.searchFloc;
      this.showSearchOptions=false;
      this.objSearchedData.Limit = this.count;
      this.objSearchedData['Key'] = 'PMAssemblyToFloc';
      this.basicService.getPmAssemblySearchData(this.objSearchedData).then((data:any)=>
      { 
        if(data.result &&  data.result.length > 0 && data.statusCode != 400){
          this.dataForSearchTable.push(...data.result);
          console.log(this.dataForSearchTable,'name=====>')
          this.showSearchOptions=false;
        }
        else if(data.result == null && data.statusCode === 400 ){
          if(this.count == 0){
            this.matOption = ["No Similar Data Searched"];
            this.showSearchOptions=true;
          }
          if(this.count > 0){
            this.stopPM = true  
            this.count = 0
          }
        }
        else{
          this.count = 0
          this.matOption = ["Error in response"];
        }
      })
    }
  }
  
  
  aPMCount=0;
  stopAPM = false;
  showAPMSearchOptions=false;
  apmOptions = []
  dataForAPMTable=[]
  serachingAPMMethod(event?){    
  //  console.log(this.searchAP,event.target.value,this.searchAP.length,'cscscs')
    if(this.searchAP.length == 0){
      this.aPMCount = 0;
      this.stopAPM = false;
      this.dataForAPMTable=[];
    }
    if(this.searchAP.length>3){
    //this.flocSearched = [];
    this.objSearchedData.Hint = this.searchAP;
      this.showAPMSearchOptions=false;
      this.objSearchedData.Limit = this.aPMCount;
      this.objSearchedData['Key'] = 'PMAssemblyForm';
      this.basicService.getPmAssemblySearchData(this.objSearchedData).then((data:any)=>
      { 
        if(data.result &&  data.result.length > 0 && data.statusCode != 400){
          this.dataForAPMTable.push(...data.result);
          this.showAPMSearchOptions=false;
        }
        else if(data.result == null && data.statusCode === 400 ){
          
          if(this.aPMCount == 0){
            this.apmOptions = ["No Similar Data Searched"];
            this.showAPMSearchOptions=true;
          }
          if(this.aPMCount > 0){
            this.stopAPM = true;  
            this.aPMCount = 0
          }
        }
        else{
          this.aPMCount = 0
          this.apmOptions = ["Error in response"];
        }
      })
    }
  }
showSearchOptions:boolean = false;
closeSearchBarOptions(){
  this.searchFloc = ''
  this.matOption = [];
  this.showSearchOptions=true;
}
closeSearchBarApOptions(){
  this.searchAP= ''
  this.apmOptions = []
  this.showAPMSearchOptions = true;

}
closeSearchBarMatOptions(){
  this.searchMaterial='';
  this.matSearchOptions = [];
  this.showMaterialSearchOptions=true;
}
  treeControl: FlatTreeControl<DynamicFlatNode>;
  dataSource: DynamicDataSource;

  getLevel = (node: DynamicFlatNode) => node.level;

  isExpandable = (node: DynamicFlatNode) => node.expandable;

  hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;

  // method to load the flocs at page load
  loading:boolean = false;
  getTreeData() {
    this.loading =true;
    this.basicService.pmAssemblyTreeViewData("").then((data: any) => {
      console.log('thisss',data);
      this.loading =false;
      if  (data && data.statusCode === 200) {
        if (data.result.treeData.length > 0) {
        
          this.database.dataMap = data.result.treeData[0].subNode;
          console.log('Tree1apiData',data.result.treeData[0].subNode);
          
          let root = data.result.treeData;
          console.log('tree1data.resultRootInitialise',data.result);
          
          root[0].subNode = [];
          this.database.rootLevelNodes = root;
          console.log('tree1Database',this.database);
          
          this.dataSource.data = this.database.initialData();
          console.log('tree1DataSource',this.dataSource.data);

          this.treeControl.expandAll();
          
        }
      }
    });
  }

  // end
subNodes=[]
async getFlatedSubNodes(subnodes){
  if (subnodes.length == 0){
    for await (let node of this.subNodes){
      node.subNode = []
    }
         return this.subNodes
  }
   if (subnodes[0] && subnodes[0].subNode.length >= 0){
    this.subNodes.push(...subnodes)
     return this.getFlatedSubNodes(subnodes[0].subNode)
   }
}
  saveEXData()  {
let exData = {};
exData = this.secondTableData
this.basicService.updateExData(exData).subscribe((data)=>{
  console.log(data,'sadcscs')
  if(data['statusCode'] === 200){
    this.basicService.openSnackBar("Update Sucessfull",'Sucess')
  }else{
    this.basicService.openSnackBar("Error While Updating ",'Sucess')
  }
})
    }
 async expandTree(data){
    this.database.dataMap = data['result']['treeData']
    console.log('lengthhh',[data['result']['treeData'][data['result']['treeData'].length-4]]);
    let len= data['result']['treeData'].length
            this.database.rootLevelNodes = [data['result']['treeData'][data['result']['treeData'].length-len]];
            this.dataSource.data = await this.database.initialData();
            console.log(this.database,this.dataSource,'casdcascs')
            for(let data of this.dataSource.data){
              this.treeControl.expandAll()
            }
  }
 flocDataUpdateAfterSearch;
 pmAssemblyTableData=[];
 matAndSubAssemblyData=[];

async validatePMAssembly(){
  this.dropDown.pmAssembly.forEach((o) => {if(o.enable)o.enable = false})
  console.log(this.dropDown.pmAssembly,'ca')
  for (let i=0;i<this.dropDown.pmAssembly.length;i++){
    for (let j=0;j<  this.pmAssemblyTableData.length;j++){
        if(this.pmAssemblyTableData[j].PMAssembly ==  this.dropDown.pmAssembly[i].PMAssembly1){
          this.dropDown.pmAssembly[i]['enable']=true;
        }       
    }
  }
}
 aPMData=[];
 pmAssemblyDeletePassed:string;
 async eventOnClick(flocPassed,toDelete?,fromSection?){
   if(flocPassed && toDelete && fromSection){
     if(fromSection === 'fromPmAssembly'){
       this.pmAssemblyDeletePassed = flocPassed;
     }
    console.log(flocPassed,'pppppp',toDelete,fromSection);
  }
  else{
   
  this.basicService.getBomTreeViewData(flocPassed).then(async(data)=>{           
    if(data['statusCode'] === 200){
      this.matAndSubAssemblyData = data['result']['SubAssemblyFormData'] 
      this.aPMData = data['result']['AssemblyToFlocFormData']
      this.pmAssemblyTableData = data['result']['pmAssembly']
      this.dropDown.floc = data['result'].flocDropDownData
      this.dropDown.floc.map(o=>{if(o.BomReqd == 'Yes'){ return o.BomReqd = true }
                                if(o.BomReqd == 'No'){return o.BomReqd = false }
                                if(o.BomReqd == null){ return o.BomReqd = false }
                            })
      
  
      console.log(this.dropDown.floc,'flooooc')
      this.validatePMAssembly();
      console.log(this.dropDown.pmAssembly,this.pmAssemblyTableData,'assemblys')
      if(toDelete != null || fromSection != null){
        this.expandTree(data)  

      }
        }
   
      }) 
  }
  }


  getPMSearchData(event){
    this.basicService.scrolledAtBottom(event).subscribe((getMoreData)=>{
      if(getMoreData){
        this.count+=5;
        if(!this.stopPM){
          this.serachingPMMethod() 
        }
      }else{
        console.log('Not at Bottom')
      }
    })
   
  }





  convertDate(date:Date){
  let month =  date.getUTCMonth() + 1;
  let parsedDate = date.getUTCDate();
  let year = date.getFullYear();
   return [month,parsedDate,year].join('/')
  
  }
  saveExData(){

  }

  // method save data of right top panel  
  sendObj={'FlocLevel':'','Floc1':'','ObjType':'','ShortText':'','ExInd':'','ABC':'','PerfStd':'','PerfStdAppDel':'','BomReqd':'','ConsType':'','SerialNo':'','SuperiorFloc':'','EDCCode':'','MaintPlant':'','StartUpDate':''};
  saveTopRightData(){
    if(this.firstTableData && this.firstTableData.FlocLevel)
    {
      this.sendObj['FlocLevel'] = this.firstTableData.FlocLevel == 'select'?'null':this.firstTableData.FlocLevel;
      this.sendObj['Floc'] =this.firstTableData.Floc1== 'select'?'null':this.firstTableData.Floc1;
      this.sendObj['ObjType'] =this.firstTableData.ObjType == 'select'?'null':this.firstTableData.ObjType;
      this.sendObj['ShortText'] =this.firstTableData.ShortText == 'select'?'null':this.firstTableData.ShortText;
      this.sendObj['ExInd'] =this.firstTableData.ExInd == 'select'?'null':this.firstTableData.ExInd;
      this.sendObj['ABC'] =this.firstTableData.ABC == 'select'?'null':this.firstTableData.ABC;
      let perfStdChange = this.firstTableData.PerfStd.toString().split(',').join(';');
      this.sendObj['PerfStd'] = perfStdChange == 'select'?'null':perfStdChange;
      this.sendObj.PerfStdAppDel =this.firstTableData.PerfStdAppDel== 'select'?'null':this.firstTableData.PerfStdAppDel;
      this.sendObj.BomReqd =this.firstTableData.BomReqd== 'select'?'null':this.firstTableData.BomReqd;
      this.sendObj.ConsType =this.firstTableData.ConsType == ' '?'null':this.firstTableData.ConsType;
      this.sendObj.SerialNo =this.firstTableData.SerialNo== 'select'?'null':this.firstTableData.SerialNo;
      this.sendObj.SuperiorFloc =this.firstTableData.SuperiorFloc== 'select'?'null':this.firstTableData.SuperiorFloc;
      this.sendObj['EDCCode'] =this.firstTableData.EDCCode == 'select'?'null':this.firstTableData.saveEXDataEDCCode;
      this.sendObj.MaintPlant =this.firstTableData.MaintPlant== 'select'?'null':this.firstTableData.MaintPlant;
      this.sendObj.StartUpDate =this.firstTableData.StartUpDate== 'select'?'null':this.firstTableData.StartUpDate;
      this.sendObj.StartUpDate = this.convertDate(new Date (this.firstTableData.StartUpDate))
     console.log(this.sendObj.StartUpDate,'dattttttteeeeeeeeeeee')
      this.basicService.saveFlocTopData(this.sendObj).then((dataRes:any)=>{
          if(dataRes.statusCode === 200 && dataRes.message === 'Success' ){
            this.basicService.openSnackBar('Data Successfully saved','Sucess');
          }
          else{
            this.basicService.openSnackBar('Try again','Unsuccessful');
          }
      })
    }
    else{
      this.basicService.openSnackBar('Fields Must have data to save','Try again');
    }

  }
  selectedTab(event){
    this.selectedTabIndex=event.index;
    console.log(event,this.selectedTabIndex,'tabSWitched')
  }
 

  cssCount:number=0;
  show:boolean=false;
  showPmAssembly(eventPassed){
    console.log('eventPasssssssss',eventPassed);
    this.show =! this.show; 
  }
  getAPMSearchData(event){
    this.basicService.scrolledAtBottom(event).subscribe((getMoreData)=>{
      if(getMoreData){
        this.aPMCount+=5;
        if(!this.stopAPM){
          this.serachingAPMMethod()
        }
      }else{
        console.log('not at Bottom')
      }
    })
   
  }


  objTypeSearchResult;
  countObjTypeOnSearch:number = 0;
  getObjTypeOnSearch(inputPassed){
    // console.log('inputPassed',inputPassed.target.value);
    if(inputPassed.length > 0){
      this.countObjTypeOnSearch = 0;
      let objPassing = {'Key':'ObjType','Hint':inputPassed,'Limit':this.countObjTypeOnSearch}
      this.basicService.postBomScreenPmAssemblyFormData(objPassing).then((dataRes:any)=>{
        if(dataRes.result!=null){
          this.objTypeSearchResult = dataRes.result.DropDownData;
          console.log('makeSearchResult2',this.objTypeSearchResult,dataRes,dataRes.result.PMAssemblyToFlocFormDropDownData);
        }
        else{
          // this.objSearchedData= [];
          console.log('makeSearchResult23',this.objSearchedData);
        }
      });
    }
    else{
      this.objSearchedData = ['Enter a character to search'];
      console.log('makeSearchResult23333',this.objSearchedData);
    }
  }
  setFun(data){
    var d= data;
    return d;
  }

  makeSearchResult;
  countMakeOnSearch:number = 0;
  getMakeOnSearch(inputPassed){
    this.countMakeOnSearch = 0;
    // console.log('inputPassedForSearch',inputPassed);
    if(inputPassed.length > 0){
      let objPassing = {'Key':'Make','Hint':inputPassed,'Limit':this.countMakeOnSearch}
      this.basicService.postBomScreenPmAssemblyFormData(objPassing).then((dataRes:any)=>{
        if(dataRes.statusCode === 200){
          this.makeSearchResult = dataRes.result.DropDownData;
         }
      });
    }
    else{
      this.makeSearchResult = ['No data found'];
    }
  }


  loadMoreObjType(event,dataBindForSearch){
    console.log('scrolllMore1');
    let pos = event.target.scrollTop + event.target.offsetHeight;
    let max = event.target.scrollHeight
    console.log("scrollll",pos,max);
        if (parseInt(pos) == max) {
          console.log('scrolllMore2',event,'=====>',dataBindForSearch);
          let dataBindForSearchPrevious;
          dataBindForSearchPrevious = dataBindForSearch;

          if( dataBindForSearchPrevious == dataBindForSearch){
            this.countObjTypeOnSearch+=15;
            let objPassing = {'Key':'ObjType','Hint':dataBindForSearch,'Limit':this.countObjTypeOnSearch}
            this.basicService.postBomScreenPmAssemblyFormData(objPassing).then((dataRes:any)=>{
              if(dataRes.result!=null){
                this.objTypeSearchResult.push(...dataRes.result.DropDownData);
                console.log('makeSearchResult2',this.objTypeSearchResult);
              }
              else if(dataRes.status == "BadRequest" || dataRes.statusCode === 400){
                this.basicService.openSnackBar('Data Not found','Error');
                this.countObjTypeOnSearch = 0;
              }
              else{

              }
            });
          }
          else{
            this.countObjTypeOnSearch = 0;
          } 
        }   
  }
 
  loadMoreMake(eventPassed,MakeValuePassed){
    let pos = eventPassed.target.scrollTop + eventPassed.target.offsetHeight;
    let max = eventPassed.target.scrollHeight
    console.log("scrollll",pos,max);
        if (pos-10 == max) {
          this.countMakeOnSearch+=15;
          let objPassing = {'Key':'Make','Hint':MakeValuePassed,'Limit':this.countMakeOnSearch}
          this.basicService.postBomScreenPmAssemblyFormData(objPassing).then((dataRes:any)=>{
            if(dataRes.result!=null){
              this.makeSearchResult.push(...dataRes.result.DropDownData);
              console.log('MakeValuePassed',  this.makeSearchResult);
            }
            else if(dataRes.status == "BadRequest" || dataRes.statusCode === 400){
                  this.basicService.openSnackBar('Data Not found','Error');
                  this.countObjTypeOnSearch = 0;
                }
            else{

                }

              });
            
        }
        else{
        }
    
  }
  clickOnTreeNode(node){
    console.log('node1',node)
    if(node && node.item && node.item.Key === "PMAssemly" || node && node.item && node.item.Key === "PMAssembly"  ){
      // console.log('loadinController11');
      let searchParameter;
      (node.item.Key === "SubAssembly")?searchParameter = node.item.Parent:'';
      (node.item.Key === "ObjType")?searchParameter = node.item.ObjType:'';
      (node.item.Key === "PMAssemly")?searchParameter = node.item.PrintValue:'';
      (node.item.Key === "PMAssembly")?searchParameter = node.item.PrintValue:'';
      (node.item.Key === "Material")?searchParameter = node.item.PrintValue:'';
      console.log('node2',searchParameter);
      
      this.eventOnClick(searchParameter);
    }
    if( node.item.Key === "ObjType"){
      debugger  
      let params = 'component';
      this.basicService.pmAssemblyTreeViewData(node.item.PrintValue,params).then((data)=>{
        // console.log('loadinController',data['statusCode'],data);
        if(data['statusCode'] == 200){
          this.pmAssemblyTableData = data['result'].pmAssembly;
       

          console.log(this.pmAssemblyTableData,'dhasjdbkadsa1')
          this.matAndSubAssemblyData =data['result'].SubAssemblyFormData;
          this.aPMData =data['result'].AssemblyToFlocFormData;
          console.log('aPMData',this.aPMData);
          this.dropDown.floc = data['result'].flocDropDownData
         // this.loadinController = false;
          this.validatePMAssembly()
        }
      })
    }
    else if( node.item.Key === "Material" || node.item.Key === "SubAssembly"){
     // this.loadinController = false;
    this.basicService.pmAssemblyTreeViewSearchedData(node.item.componentID).then((response)=>{
      if(response['statusCode'] === 200){
        this.pmAssemblyTableData = response['result'].pmAssembly;
        this.matAndSubAssemblyData =response['result'].SubAssemblyFormData;
         this.aPMData =response['result'].AssemblyToFlocFormData;
      
         this.dropDown.floc = response['result'].flocDropDownData
        // this.expandTree(response)  
        
         this.validatePMAssembly()
      }
    })
    }

    else if(node.item.Key === "Parent"){
      //this.loadinController = false;
    }
    
  }

    collapseAll(){
      this.treeControl.collapseAll();
    }
    eventOnRefreshClick(){
      this.basicService.pmAssemblyTreeViewData('').then((data:any)=>{
        if(data && data.message =='Success'){
          this.getTreeData();
        }
        this.dropDownValues=data;
        console.log("data for dropDown Values @@@==>",data,this.dropDownValues);
        });
    }
    setVAlues(data,index){
      this.aPMData[index].BomDesc = this.dropDown.pmAssembly.find(o=>o.PMAssembly1 == data.PMAssembly).ShortText;
      console.log(data,'ccscss')
    }
    componentSearchResult;
    componentSearch(hintPassed){
      this.basicService.searchBomScreenDropDownData(hintPassed).then((dataResponse:any)=>{
        console.log('dataResponse',dataResponse.result.ComponentDropDownData);
        this.componentSearchResult = dataResponse.result.ComponentDropDownData
      })
    }


    
    setValues(data,index){
      this.aPMData[index].FlocDesc = this.dropDown.floc.find(o=>o.Floc1 == data.FLOC).ShortText;
    }





    rowDataChanging(event,data,index,changesArray){
      console.log('row',event.target.value,data,index,changesArray);
      changesArray[index]=data;
      console.log( this.pmAssemblyChanges,this.matAndSubChanges,'test=====')
    } 





    // save button functionality of 3 section start
    savePmAssemblyData(){
      console.log('savePmAssemblyData',this.pmAssemblyChanges);

          this.callToSaveRowDataOfPmAssembly(this.pmAssemblyTableData);
        

      }
   
    

    callToSaveRowDataOfPmAssembly(dataObjToSave){
      dataObjToSave.forEach((data)=>{
        if(data.plant && Array.isArray(data.plant)){
          delete data.plant
        }
      })
      this.basicService.BomScreenPMAssemblyFormData(dataObjToSave).then((response:any)=>{
        if(response.statusCode == 200){
          console.log('response',response);
          this.matAndSubChanges = []
          console.log('afterResponse',this.matAndSubChanges);
          this.basicService.openSnackBar('Data updated Successfully','Success');            
        }
        else if(response.statusCode == 400 ){
          this.basicService.openSnackBar('Data Not updated','Response Error');
        }
        else{
          this.basicService.openSnackBar('Response Error','Error');
        }
      });  
    }




    savePmSubAssemblyData(){

     this.callMethodToSaveRowData(this.matAndSubAssemblyData);
    
      
    }

    callMethodToSaveRowData(dataObjToSave){
      if(dataObjToSave.length != 0){
        dataObjToSave.forEach((data)=>{
          if(data.plant) delete data.plant
        })
      }
      this.basicService.BomScreenMaterialAndSubAssemblyFormDataSavePMAssembly(dataObjToSave).then((response:any)=>{
        if(response.statusCode == 200){
          console.log('response',response);
          this.matAndSubChanges = []
          console.log('afterResponse',this.matAndSubChanges);
          this.basicService.openSnackBar('Data updated Successfully','Success');            
        }
        else if(response.statusCode == 400 ){
          this.basicService.openSnackBar('Data Not updated ','Response Error');
        }
        else{
          this.basicService.openSnackBar('Response Error','Error');
        }

      });
      
    }

    savePmAssociateAssemblyData(){
   
        this.saveRowDataOfAssociatePmAssembly(this.aPMData);
        // this.basicService.BomScreenPMAssemblyToFlocFormDataSaveAssemblyToFloc(filtered).then((response:any)=>{
        //   if(response.statusCode == 200){
        //     console.log('response',response);
        //     this.apmAssemblyChanges = []
        //     console.log('afterResponse',this.apmAssemblyChanges);
        //     this.basicService.openSnackBar('Data updated Successfully','Success');            
        //   }
        //   else if(response.statusCode == 400 ){
        //     this.basicService.openSnackBar('Data Not updated','Error');
        //   }
        //   else{
        //     this.basicService.openSnackBar('Response Error','Error');
        //   }

        // });

      }
     

    
    saveRowDataOfAssociatePmAssembly(dataObjToSave){
      console.log(dataObjToSave,'datatotoototoot')
      if(dataObjToSave.length > 0){
      this.basicService.BomScreenMaterialAndSubAssemblyFormDataSavePMAssembly(dataObjToSave).then((response:any)=>{
        if(response.statusCode == 200){
          console.log('response',response);
          this.matAndSubChanges = []
          console.log('afterResponse',this.matAndSubChanges);
          this.basicService.openSnackBar('Data updated Successfully','Success');            
        }
        else if(response.statusCode == 400 ){
          this.basicService.openSnackBar('Data Not updated','Response Error');
        }
        else{
          this.basicService.openSnackBar('Response Error','Error');
        }

      });
    }else{
      this.basicService.openSnackBar('No data to save',true);
    }
      
    }

    // save button functionality of 3 section end





    // adding & removing rows from table methods start
    pmAssemblyRowRemove(){

      if(this.pmAssemblyTableData.length > 0)
      {
        console.log('thissssss',this.pmAssemblyTableData,this.pmAssemblyTableData[this.pmAssemblyTableData.length-1].key);
        // (this.pmAssemblyTableData,this.pmAssemblyTableData.length - (this.pmAssemblyTableData.length-1 ))
        if(this.pmAssemblyTableData &&  this.pmAssemblyTableData[this.pmAssemblyTableData.length-1].key ){
          this.pmAssemblyTableData.pop();
          this.basicService.openSnackBar('Row removed','');
        }
        else{
          this.basicService.openSnackBar('No added rows from user to remove','Try again');
        }
      }
      else{
        this.basicService.openSnackBar('No row in table to remove','Try again');
      }
    }

    pmAssemblyRowAdd(){
      let tempObj={
        Make: "",
        Model: "",
        ObjType: "",
        PMAssembly: "",
        Plant: "",
        RefNo: '',
        Rev: '',
        ShortText: "",
        key:'rowAddOn'
      };
      console.log('sssscddddddddddddddd')
    //  if(!this.pmAssemblyTableData.length) {
    
    //}
      this.pmAssemblyTableData.push(tempObj);
      this.basicService.openSnackBar('Row added','');
    
    }

    subAssembliesRowRemove(){
      console.log('matAndSubAssemblyData',this.matAndSubAssemblyData,this.matAndSubAssemblyData.length);

    
      if(this.matAndSubAssemblyData && this.matAndSubAssemblyData.length > 0)
      {
        if(this.matAndSubAssemblyData && this.matAndSubAssemblyData[this.matAndSubAssemblyData.length-1].key ){
          this.matAndSubAssemblyData.pop();
          this.basicService.openSnackBar('Row removed','');
        }
        else{
          this.basicService.openSnackBar('No added rows from user to remove','Try again');
        }
      }
      else{
        this.basicService.openSnackBar('No row in table to remove','Try again');
      }
     
    }
    subAssembliesRowAdd(){
      // this.matAndSubAssemblyData = []; for searching
      let tempObj={
        BomAddDesc1: '',
        BomAddDesc2: '',
        Component: "",
        ComponentDesc: "",
        OLTO: '',
        PMAssembly: "",
        Parent: "",
        PlanningPlant: '',
        Quantity: '',
        RefNo: '',
        SeqNo: '',
        UOM: "",
        key:'rowAddOn'
      };
     
       if(this.matAndSubAssemblyData.length == 0){
        this.dropDown.pmAssembly.forEach((obj)=>{
          console.log(obj,'scddddddddddddddd')
        obj.enable = true
      });
      }
      this.matAndSubAssemblyData.push(tempObj);
      this.basicService.openSnackBar('Row added','');
    }

    PmAssociateAssemblyRowRemove(){
      if(this.aPMData.length > 0)
      {
        if(this.aPMData && this.aPMData[this.aPMData.length-1].key){
          this.aPMData.pop();
          this.basicService.openSnackBar('Row removed','');
        }
        else{
          this.basicService.openSnackBar('No added rows from user to remove','Try again');
        }
      }
      else{
        this.basicService.openSnackBar('No row in table to remove','Try again');
      }

    }
    PmAssociateAssemblyRowAdd(){
      let tempObj={
        BomDesc: "",
        FLOC: "",
        FlocDesc: "",
        PMAssembly: '',
        key:'rowAddOn'
      }
      this.aPMData.push(tempObj);
      this.basicService.openSnackBar('Row added','');
    }
        // adding & removing rows from table methods end




  // flagCall method to check the component is selected from dropdown or not
  // flag={'flagValue':'','index':''};
  // flagCall(checkValue,index){
  //   console.log('flag1',this.flag,'--',index,this.flag[index]);
    
  //   this.flag[index].flagValue = checkValue;
  //   this.flag[index].index = index;
  //   console.log('flag2',this.flag);

  // }

  materialSearchResult;
  countManufacturer = 0;
  searchHintForManufacturerCode:string;
  manufacturerCodeSearch(eventPassed){
    this.apiCallForManufacturerDropDown = true;
    this.countManufacturer = 0;
    this.searchHintForManufacturerCode = eventPassed;
    let obj = {
      "Hint":eventPassed,
        "Key":"manufactuerCode",
        "Limit": this.countManufacturer
    }
    if(eventPassed.length>2){
      this.basicService.searchManifacturerCode(obj).then((data)=>{
        console.log(data,'csscss')
       if(data['statusCode'] == 200) this.materialSearchResult=data['result'].ComponentDropDownData
      })


    }
  }

  // load More DropDown on Scroll of PmAssembly->ManufactureCode
  apiCallForManufacturerDropDown:boolean = true;
  loadMoreManufacturerCodeOnScroll(eventPassed){

    let pos = eventPassed.target.scrollTop + eventPassed.target.offsetHeight;
    let max = eventPassed.target.scrollHeight
    console.log("scrollll",eventPassed.target.value);
        if (parseInt(pos) == max ) {
          if(this.apiCallForManufacturerDropDown){

            this.countManufacturer+=15;
            let obj = {
              "Hint":this.searchHintForManufacturerCode,
                "Key":'manufactuerCode',
                "Limit": this.countManufacturer
            }
            this.basicService.searchManifacturerCode(obj).then((response)=>{
              if(response['statusCode'] == 200 && this.apiCallForManufacturerDropDown ){
                if(response['result'] && response['result'].ComponentDropDownData && response['result'].ComponentDropDownData.length !< 15)
                {
                  console.log('log',response['result'].ComponentDropDownData.length);
                  this.apiCallForManufacturerDropDown = false;
  
                }
                this.materialSearchResult = [...this.materialSearchResult,...response['result'].ComponentDropDownData];
              }
              else{
                this.basicService.openSnackBar('No Response','Error');
              }
            })
          }
          else{
            this.basicService.openSnackBar('No More Data for dropdown','');
          }
       
        }
        else{
               }

  }

  // delete PmAssembly 
  pmAssemblyDelete(){
    console.log(this.pmAssemblyChanges,'changeeeeesssss')
    if(this.pmAssemblyChanges.length == 0) return this.basicService.openSnackBar("Please Select a PM Assembly To Delete",false)
    this.basicService.bomScreenPmAssemblyData(this.pmAssemblyChanges[this.pmAssemblyChanges.length-1].PMAssembly).then((response:any)=>{
      if(response.statusCode == 200){
        //console.log();
        let index =  this.pmAssemblyTableData.findIndex(o=>o.PMAssembly == this.pmAssemblyChanges[this.pmAssemblyChanges.length-1].PMAssembly);
         if(index > -1)  this.pmAssemblyTableData.splice(index,1)
        this.getTreeData()
        this.basicService.openSnackBar('Delete Successfully','Success');
      }
      else{
        this.basicService.openSnackBar('Not Deleted','Error');
      }

    });
    console.log();
  }

    // delete PmSubAssembly 
  pmSubAssemblyDelete(){
    if(this.matAndSubChanges.length != 0){
      this.basicService.bomScreenMaterialAndSubAssemblyFormData(this.matAndSubChanges[this.matAndSubChanges.length-1].Component).then((response:any)=>{
        if(response.statusCode == 200){
          this.matAndSubAssemblyData = [];
          this.getTreeData();
          this.basicService.openSnackBar('Deleted Successfully','Success');
        } 
        else{
          this.basicService.openSnackBar('Not Deleted','Error');
        } 
      });
    }
    else{
      this.basicService.openSnackBar('No Row Selected To Delete','Error');
    }
    console.log();
  }

    // delete PmAssociateAssembly 
    apmChanges:Array<object> = []
  pmAssociateAssemblyDelete(){
    console.log('this.associatePmAssemblyDelete',this.associatePmAssemblyDelete);
    if(this.apmChanges.length != 0){
      this.basicService.bomScreenAssociatePMAssemblyToFlocData(this.apmChanges[this.apmChanges.length-1]['PMAssembly'],this.apmChanges[this.apmChanges.length-1]['FLOC']).then((response:any)=>{
    
          if(response.statusCode === 200){
           
            this.getTreeData()
            this.basicService.openSnackBar('Successfully Deleted','Success');
            console.log('response',response);            
          }
          // console.log('response',response);
        
        else{
          this.basicService.openSnackBar('No Response','Error');
          console.log('responseErr',response);
        }
      })
    }
  }


}

 




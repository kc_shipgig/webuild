import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Subscription, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs'
import { Observable } from "rxjs"
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})

export class BasicService {

  // for Server deployment uncomment it
  //baseUrl='http://192.168.1.70/webbuild/';
  toggleNode = new Subject();
  //for test server
 baseUrl = `${window.location.protocol + "//" + window.location.hostname}/api/`

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {

    this.token.accessToken = "Bearer " + sessionStorage.getItem('access_token')
    this.token.refreshToken = sessionStorage.getItem('refresh_token')
  }
  openSnackBar(message: any, action: any) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  token = { 'accessToken': '', 'refreshToken': '' }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': "Bearer " + sessionStorage.getItem('access_token'),
    })
  };


  loginUser(data) {
    try {
      // console.error('passwordERROR01')
      return this.http.post(this.baseUrl + "WBLogin", data);
    } catch (e) {
      // console.error('passwordERROR', e)
      if (e) {
        this.openSnackBar('e.error', 'e.error_description');
      }
    }
  }

  // API to get the info about the floc child
  treeViewData(floc) {
    try {
      return this.http.get(this.baseUrl + "api/TreeViewData?floc=" + floc).pipe(map((res: any) => this.extractData(res)));
    } catch (e) {
      console.error(e)
    }
  }

  // API give info about the floc and related data of exData,documentData
  treeViewFlocData(floc) {
    return this.http.get(this.baseUrl + "api/TreeViewFlocData?floc=" + floc).toPromise().catch(e => console.log("error", e))
  }


  //search data for floc
  getPmAssemblySearchData(aseemblyData) {
    return this.http.post(this.baseUrl + 'api/SearchDataForBomScreenForm', aseemblyData).toPromise().catch(e => {
      console.error('error while searchingFlock', e.message)
    })
  }

  scrolledAtBottom(event): Observable<any> {
    let pos = event.target.scrollTop + event.target.offsetHeight;
    let max = event.target.scrollHeight
    console.log(pos, max, pos == max, 'loggingMax')
    if (parseInt(pos) == max) {
      return of(true)
    } else {
      return of(false)
    }
  }
  private extractData(res: Response) {
    let body = res;
    return body || {};
  }
  // API for the dropDown 
  listFlocBasicData() {
    //  console.log("knfjwnefjb",this.token);
    return this.http.get(this.baseUrl + "api/ListFlocBasicData").toPromise().catch(e => { console.log(e) });
  }
  //  getPopDataOfTable
  formDocumentData(dataLimit) {

    return this.http.get(`${this.baseUrl}api/FormDocumentData?startLimit=${dataLimit}`).toPromise().catch(e => {
      console.log(e)
    });
  }

  // to search on right top panel
  searchFloc(flocWithStartLimit) {
    return this.http.post(this.baseUrl + "api/SearchFloc", flocWithStartLimit).toPromise().catch(e => { console.log(e) });
  }
  updateDocumentData(flockDAta) {
    return this.http.post(this.baseUrl + "api/SaveFlocDocsData", flockDAta).toPromise().catch(e => { console.log(e) });
  }

  // to hit WBLogin again on authorization
  refreshTokenGet() {
    let data = "grant_type=refresh_token&refresh_token=" + sessionStorage.getItem('refresh_token');
    try {
      return this.http.post(this.baseUrl + "WBLogin", data).toPromise();
    } catch (e) {
      console.error(e)
    }
  }
  getSubAssemblyAndMatDAta(component) {

    return this.http.get(this.baseUrl + `PMAssemblyTreeViewSearchedData/Component?component=${component}&number=1`).toPromise().catch(e => { console.log(e) })
  }
  getAssosiateAssembly(counter) {
    console.log('inside calssss')
    return this.http.get(this.baseUrl + `api/BomScreenPMAssemblyToFlocFormData?leftRecord=${counter}`).toPromise().catch(e => { console.log(e) })
  }

  // API for the Xls Data to save on server
  saveFlocDataFromCsv(data) {
    console.log("datafile", data);
    return this.http.post(this.baseUrl + 'api/SaveFlocDataFromCSV', data).toPromise().catch(e => { console.log(e) })
  }

  // API for downloading xls sheet of search data
  exportData(floc) {
    console.log(floc, 'herre')
    return this.http.get(`${this.baseUrl}api/ExportSearchResult?flocHint=${floc}`).toPromise().catch((error: any) => console.log(error))
  }

  // calling the initialCall method for calling 2 API in interceptor
  callingInitialCallEventObject = new EventEmitter();
  subsVar: Subscription;
  callingInitialCall() {
    console.log('============>service');
    // new HomeComponent();
    return this.callingInitialCallEventObject;
  }
  // exData
  updateExData(exData) {
    try {
      return this.http.post(this.baseUrl + 'api/SaveFlocBottomExData', exData);
    } catch (e) {
      console.error('error occured:', e.message)
    }

  }
  updateOtherFlocData(otherFlocData) {
    try {
      return this.http.post(this.baseUrl + 'api/SaveFlocBottomOtherFlocData', otherFlocData);
    } catch (e) {
      console.error('error occured:', e.message)
    }
  }
  saveFlocTopData(ObjPassed) {
    console.log('saveTopRightData', ObjPassed);
    return this.http.post(this.baseUrl + 'api/SaveFlocTopData', ObjPassed).toPromise().catch((error: any) => { console.log(error); })
  }
  searchExploreNodeData(flocPassed) {
    return this.http.get(this.baseUrl + 'api/SearchExploreNodeData?selectedFloc=' + flocPassed).toPromise().catch((errorResponse: any) => { console.log(errorResponse); });
  }
  searchFormDocumentDropListData(typePassed) {
    return this.http.post(this.baseUrl + 'api/SearchFormDocumentDropListData', typePassed).toPromise().catch((error: any) => { console.log('error', error); });
  }

  getBomScreenPmAssemblyFormData(counter) {

    return this.http.get(this.baseUrl + `api/BomScreenPMAssemblyFormData?lr=${counter}`).toPromise().catch((error: any) => { console.log('error', error); });
  }
  // getMaterialAndSubAssemblyData(counter){
  //   return this.http.get(this.baseUrl+`api/BomScreenMaterialAndSubAssemblyFormData?leftRecord=${counter}`)
  // }
  postBomScreenPmAssemblyFormData(searchParam) {
    return this.http.post(this.baseUrl + 'api/SearchBomScreenDropdownData', searchParam).toPromise().catch((error: any) => { console.log('error', error); });
  }

  getBomTreeViewData(searchedContent) {
    return this.http.get(this.baseUrl + `api/PMAssemblyTreeViewSearchedData?pmAssembly=${searchedContent}`).toPromise().catch((error: any) => {
      console.log('error', error);
    })
  }

  getBomScreenSelectorData() {
    return this.http.get(this.baseUrl + `api/SearchBomScreenDropdownData`).toPromise().catch((error: any) => {
      console.error('error', error);
    })
  }
  setToggleData(data) {
    this.toggleNode.next(data)
  }
  addSubAssembly(subAssemblyData) {
    return this.http.post(this.baseUrl + 'api/AddMaterialAndSubassembly', subAssemblyData).toPromise().catch((error: any) => { console.log('error', error); });
  }

  pmAssemblyTreeViewData(keyPassed, key?,para?) {
    console.log('keyPassed', keyPassed,para);
    if (keyPassed!= null  && para != null ) {
      return this.http.get(this.baseUrl + `api/PMAssemblyTreeViewData?${para}=` + JSON.stringify(keyPassed)).toPromise().catch((error: any) => { console.log('error', error); });
    }
    else if (key === 'PMAssemly' ) {
      return this.http.post(this.baseUrl + 'api/PMAssemblyTreeViewData?pmAssembly=' + keyPassed, keyPassed).toPromise().catch((error: any) => { console.log('error', error); });
    } else {
      return this.http.get(this.baseUrl + 'api/PMAssemblyTreeViewData?assembly=' + keyPassed).toPromise().catch((error: any) => { console.log('error', error); });
    }
  }


  // Get DropDown data for PmAssembly
  getPmAssemblyDropDownData() {
    return this.http.get(this.baseUrl + 'api/SearchBomScreenDropdownData').toPromise().catch((errorAny: any) => { console.log(errorAny) });
  }


  // In Material & SubAssemblies for component Search
  searchBomScreenDropDownData(hintPassed, Key = "MandSComponent", limit = 0) {
    let ObjPassed = {
      "Key": Key,
      "Hint": hintPassed,
      "Limit": limit
    }


    return this.http.post(this.baseUrl + 'BomScreenMaterialAndSubAssemblyFormData/Get', ObjPassed).toPromise().catch((error: any) => { console.log(error); })
  }




  // api to save the PmAssemblyRows
  BomScreenPMAssemblyFormData(rowDataPasssedForPmAssembly) {
    return this.http.post(this.baseUrl + 'BomScreenPMAssemblyFormData/savePMAssemblyData', rowDataPasssedForPmAssembly).toPromise().catch((anyError: any) => { console.log(anyError); })
  }

  // api to save the pmSubAssemblyRows
  BomScreenMaterialAndSubAssemblyFormDataSavePMAssembly(rowDataPasssedForSubAssembly) {
    return this.http.post(this.baseUrl + 'BomScreenPMAssemblyToFlocFormData/saveAssemblyToFloc', rowDataPasssedForSubAssembly).toPromise().catch((errorResponse: any) => { console.log(errorResponse) });
  }


  // api to save the pmAssociateAssemblyRows
  BomScreenPMAssemblyToFlocFormDataSaveAssemblyToFloc(rowDataPasssedForAssociateAssembly) {
    return this.http.post(this.baseUrl + 'BomScreenPMAssemblyToFlocFormData/saveAssemblyToFloc', rowDataPasssedForAssociateAssembly).toPromise().catch((errorCatch: any) => { console.log(errorCatch); });
  }


  // to search Manufacture Code on input property  && to show more dropdown result on reaching scroll at bottom of PmAssemblyView->PmAssemblyTable->ManufactureCode
  searchManifacturerCode(searchKey) {
    return this.http.post(this.baseUrl + 'BomScreenMaterialAndSubAssemblyFormData/Get', searchKey).toPromise().catch((errorCatch: any) => { console.log(errorCatch); });
  }
  //clickParticular Assosiate PmAssembly data


  getParticularAssociatePmAssemblyData(searchObj) {
    return this.http.get(this.baseUrl + `api/BomScreenPMAssemblyToFlocFormData?floc=${searchObj.floc}&pmAssembly=${searchObj.pmAssembly}`).toPromise().catch((errorCatch: any) => { console.log(errorCatch); });
  }


  // api to delete the PmAssembly
  bomScreenPmAssemblyData(pmAssemblyPassedToDelete) {
    let ObjPassed = { pmAssembly: pmAssemblyPassedToDelete }
    return this.http.post(this.baseUrl + 'BomScreenPMAssemblyData/Delete', ObjPassed).toPromise().catch((errorAny: any) => { console.log(errorAny); });
  }

  // api to delete the content in PmSubAssembly
  bomScreenMaterialAndSubAssemblyFormData(componentPassedToDelete) {
    let ObjPassed = { component: componentPassedToDelete }
    return this.http.post(this.baseUrl + 'BomScreenMaterialandSubAssemblyFormData/Delete', ObjPassed).toPromise().catch((errorAny: any) => { console.log(errorAny); });
  }


  // api to delete the content in PmAssociateAssembly yhi h
  bomScreenAssociatePMAssemblyToFlocData( pmAssemblyPassed,flocPassed) {
    let obj = {
      Floc: flocPassed,
      PMAssembly: pmAssemblyPassed
    }
    // BomScreenAssociatePMAssemblyToFlocData
    return this.http.post(this.baseUrl + 'BomScreenAssociatePMAssemblyToFlocData/Delete', obj).toPromise().catch((errorAny: any) => { console.log(errorAny); });
  }


  // document popUpTable Save Row-1
  flocScreenDocumentFormDataSave(objPass) {
    let ObjPassed = {
      DocID: objPass.docID,
      SourceID: objPass.SourceID,
      ObjType: objPass.ObjType,
      DocDesc: objPass.DocDesc,
      DocType: objPass.DocType,
      DocRev: objPass.DocRev
    }
    return this.http.post(this.baseUrl + 'FlocScreenDocumentFormData/save', ObjPassed).toPromise().catch((errorAny: any) => { console.log(errorAny); });
  }

  // document popUpTable Delete Row-1
  flocScreenDocumentFormDataDelete(sourceID) {
    // let ObjPassed = { sourceId: sourceID }
    return this.http.post(this.baseUrl + `FlocScreenDocumentFormData/delete?sourceId=${sourceID}`, sourceID).toPromise().catch((errorAny: any) => { console.log(errorAny); });
  }



  // for Material & SubAssembly in second screen Tree-2
  pmAssemblyTreeViewSearchedData(passParameter){
    // PMAssemblyTreeViewSearchedData/ComponentData?component=10299699&number=1
    return this.http.get(this.baseUrl+`PMAssemblyTreeViewSearchedData/ComponentData?component=${passParameter}&number=1`).toPromise().catch((e)=>{console.log(e)})
  }


  // for deleting a floc in first screen top section on delete icon click =>screen 1
  // api/DeleteFlocScreenData?FlocId=AU04.sds
  deleteFlocScreenData(flocPassed){
    return this.http.post(this.baseUrl+`DeleteFlocScreenData/delete?FlocId=${flocPassed}`,flocPassed).toPromise().catch(console.error)
  }

}

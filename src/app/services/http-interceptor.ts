
import { Injectable ,ApplicationRef} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, finalize } from 'rxjs/operators';
import { BasicService } from './basicService';

@Injectable()
export class HTTPStatus {
public showLoader: BehaviorSubject<boolean>;
constructor() {
this.showLoader = new BehaviorSubject(false);
}

setHttpStatus(inFlight: boolean) {
this.showLoader.next(inFlight);
}

getHttpStatus(): Observable<boolean> {
return this.showLoader.asObservable();
}
}


@Injectable()
export class MyHttpInterceptor implements HttpInterceptor{
data:any={};
requestCounter=0
responseCounter=0
constructor(private httpstatus:HTTPStatus,private basicService:BasicService,private aR:ApplicationRef){

}
intercept(req:HttpRequest<any>, next:HttpHandler):Observable<HttpEvent<any>>{
if(!req.url.includes('/SearchFloc') && !req.url.includes('FormDocumentData') && !req.url.includes('TreeViewData') && !req.url.includes('TreeViewFlocData'))
{
this.httpstatus.setHttpStatus(true)
}


this.requestCounter+=1;
const cloned= req.clone({ 
});
return next.handle(cloned).pipe(tap(
(event:any)=>{ 
 if(event instanceof HttpResponse){
    this.responseCounter+=1  
    this.aR.tick();
    } 

},
err=>{ 
    if(event instanceof HttpErrorResponse){

        this.responseCounter+=1
        this.aR.tick();
    }
    if(this.requestCounter == this.responseCounter){
        console.log('stop loader');
        this.httpstatus.setHttpStatus(false)
        this.aR.tick();
        this.requestCounter = 0 ;
        this.responseCounter = 0;
        
        } 
if(err.status == 401 || err.status === 400){
this.basicService.openSnackBar('Server Unable to process request','Try again');
// code for requesting an API
//this.data= "grant_type=refresh_token&refresh_token="+sessionStorage.getItem('refresh_token') 
}


}
),finalize(()=>{
console.log('stop loader');
if(this.requestCounter == this.responseCounter){
console.log('stop loader');

this.httpstatus.setHttpStatus(false)
this.aR.tick();
this.requestCounter = 0 ;
this.responseCounter = 0;

} 
})
);
}

}

//}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlocScreenComponent } from './floc-screen.component';

describe('FlocScreenComponent', () => {
  let component: FlocScreenComponent;
  let fixture: ComponentFixture<FlocScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlocScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlocScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

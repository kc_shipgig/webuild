
import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable,ViewChild,HostListener, ChangeDetectionStrategy, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BasicService } from '../services/basicService';
import {MatDialog, MAT_DATE_FORMATS} from '@angular/material';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { FlocbottomdocumentmodalComponent } from '../flocbottomdocumentmodal/flocbottomdocumentmodal.component';
import { MatMenuTrigger } from '@angular/material';
import {HTTPStatus} from '../services/http-interceptor'
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as $ from 'jquery';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
// end for excel file
export interface Food {
  value: string;
  viewValue: string;
}
export interface PeriodicElement2 {
  ObjType: string;
  Floc: string;
  ShortText: string;
  PerfStd: string;
  SuperiorFloc: string;
  EDCCode: string;
  ConsType: string;

}
export interface PeriodicElement4 {
  tabledatashow: string;
  objtype: string;
  pmassembly: string;
  make: string;
  model: string;
  shortext: string;
  action: string;
}

export interface PeriodicElement5 {
  tabledatashow: string;
  pmassembly: string;
  component: string;
  componentdescription: string;
  olto: string;
  qty: string;
  parent: string;
  seqid: string;
  action: string;
}
export interface PeriodicElement6 {
  tabledatashow: string;
  pmassembly: string;
  pmassemblydescription: string;
  floc: string;
  flocdescription: string;
  action: string;
}

const ELEMENT_DATA4: PeriodicElement4[] = [
  {tabledatashow: '',objtype: '', pmassembly: '', make: '', model: '', shortext: '', action: ''},
  {tabledatashow: '',objtype: '', pmassembly: '', make: '', model: '', shortext: '', action: ''},
  {tabledatashow: '',objtype: '', pmassembly: '', make: '', model: '', shortext: '', action: ''}
];
const ELEMENT_DATA5: PeriodicElement5[] = [
  {tabledatashow: '',pmassembly: '', component: '', componentdescription: '', olto: '', qty: '', parent: '', seqid: '', action: ''},
  {tabledatashow: '',pmassembly: '', component: '', componentdescription: '', olto: '', qty: '', parent: '', seqid: '', action: ''},
  {tabledatashow: '',pmassembly: '', component: '', componentdescription: '', olto: '', qty: '', parent: '', seqid: '', action: ''}
];
const ELEMENT_DATA6: PeriodicElement6[] = [
  {tabledatashow: '',pmassembly: '', pmassemblydescription: '', floc: '', flocdescription: '', action: ''},
  {tabledatashow: '',pmassembly: '', pmassemblydescription: '', floc: '', flocdescription: '', action: ''},
  {tabledatashow: '',pmassembly: '', pmassemblydescription: '', floc: '', flocdescription: '', action: ''}
];



export interface TreeNode {
  floc: string,
  superiorFloc: string,
  shortText: string,
  flocLevel: string,
  isChild: boolean,
  subNode: [],
  childCount:any,
  item:any,

}
export interface DialogData {
  animal: string;
  name: string;
}
export interface Food {
  value: string;
  viewValue: string;
}

export interface TreeNode2 {
  item:any,
  floc: string,
  superiorFloc: string,
  shortText: string,
  flocLevel: string,
  isChild: boolean,
  subNode: [],
  childCount:any;
  
}

export interface DialogData {
  animal: string;
  name: string;
}
export class DynamicFlatNode {
  constructor(public item: TreeNode, public level = 1, public expandable = false,public isLoading = false) {

     }
}
export class DynamicDatabase {

  dataMap: TreeNode[] = [];
  rootLevelNodes: TreeNode[] = [];

  /** Initial data from database */
  initialData(): DynamicFlatNode[] {
    console.log('treeinitialData()',this.rootLevelNodes);
    return this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true));
  }

  getChildren(node: TreeNode): TreeNode[] | undefined {
    return this.dataMap.filter((n: TreeNode) => {
      return n.superiorFloc === node.floc
    })
  }

  isExpandable(node: TreeNode): boolean {
    console.log('casdcasdcascs11',node.isChild)
    // let HasChild = this.getChildren(node);
    // return HasChild != undefined ? HasChild.length > 0 : false;
    return node.isChild;
  }
}
export class DynamicDatabase2 {

  dataMap: TreeNode[] = [];
  rootLevelNodes: TreeNode[] = [];

  /** Initial data from database */
  initialData(): DynamicFlatNode[] {
 
    return this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true));
  }

  getChildren(node: TreeNode): TreeNode[] | undefined {
    return this.dataMap.filter((n: TreeNode) => {
      return n.superiorFloc === node.floc
    })
  }

  isExpandable(node: TreeNode): boolean {
    // let HasChild = this.getChildren(node);
    // return HasChild != undefined ? HasChild.length > 0 : false;
    return node.isChild;
  }
}
/**
 * File database, it can build a tree structured Json object from string.
 * Each node in Json object represents a file or a directory. For a file, it has filename and type.
 * For a directory, it has filename and children (a list of files or directories).
 * The input will be a json object string, and the output is a list of `FileNode` with nested
 * structure.
 */
@Injectable()
// @Input('matTreeNodeDefWhen')
 
export class DynamicDataSource {
  name: string;

  children: any;
  childCount:any;

  dataChange = new BehaviorSubject<DynamicFlatNode[]>([]);

  get data(): DynamicFlatNode[] { return this.dataChange.value; }
  set data(value: DynamicFlatNode[]) {
    this.treeControl.dataNodes = value;
    this.dataChange.next(value);
  }

  constructor(private treeControl: FlatTreeControl<DynamicFlatNode>,
  private database: DynamicDatabase, public apiService: BasicService) { }

  connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {
    this.treeControl.expansionModel.onChange.subscribe(change => {
      if ((change as SelectionChange<DynamicFlatNode>).added ||
        (change as SelectionChange<DynamicFlatNode>).removed) {
        this.handleTreeControl(change as SelectionChange<DynamicFlatNode>);
      }
    });

    return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
  }

  /** Handle expand/collapse behaviors */
  handleTreeControl(change: SelectionChange<DynamicFlatNode>) {
    if (change.added) {
      change.added.forEach(node => this.toggleNode(node, true));
    }
    if (change.removed) {
      change.removed.slice().reverse().forEach(node => this.toggleNode(node, false));
    }
  }

  /**
   * Toggle the node, remove from display list
   */
  toggleNode(node: DynamicFlatNode, expand: boolean) {
    console.log('clickTOggle');
    this.children = this.database.getChildren(node.item);
    node.isLoading = true;
    if (this.children.length == 0 && node.item.isChild && expand) {
      this.apiService.treeViewData(node.item.floc).subscribe((data: any) => {
        node.isLoading = false;
        if (data.statusCode === 200) {
          this.children = data.result;
          this.addChild(node, expand);
        }
        else if(data.statusCode == 400){
          this.apiService.openSnackBar('Floc Data not found','try again');
        }
        else{

        }
      })

    }
    else {
      this.addChild(node, expand);
    }

  }

  private addChild(node: DynamicFlatNode, expand: boolean) {

    const index = this.data.indexOf(node);
    if (!this.children || index < 0) { // If no children, or cannot find the node, no op
      return;
    }

    if (expand) {
      const nodes = this.children.map(name =>
        new DynamicFlatNode(name, node.level + 1, this.database.isExpandable(name)));
      this.data.splice(index + 1, 0, ...nodes);
    } else {  
      let count = 0;
      for (let i = index + 1; i < this.data.length
        && this.data[i].level > node.level; i++ , count++) { }
      this.data.splice(index + 1, count);
    }

    this.dataChange.next(this.data);
    node.isLoading = false;
  }

}



// mat menu context interface
export interface Item {
  id: number;
  name: string;
}
// end mat menu context





export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  ex: string;
  abc: string,
  perfstd: string,
  perfstdappdel: string,
  bomreqd: string,
  constype: string,
  serialno: string,
  superfloc: string,
  edccode: string,
  maintplant: string,
  startupdate: string,
}
export interface PeriodicElement1{
  first:'',
  second:'',
  third:'',
  fourth:'',
  fifth:''
}

export interface Dummy {
  id: number;
  value:string;HTTPStatus
  viewValue:string;
}
export interface dropdata {
  value: string;
  viewValue: string;
  id:number;
}
interface IOption {
  value: string;
  label: string;
  disabled?: boolean;
}
class MyOption implements IOption {
  value: string;
  label: string;
  state: string;
}

export const DD_MM_YYYY_Format = {
  parse: {
      dateInput: 'LL',
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
  },
};
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', ex: '', abc: '', perfstd: '', perfstdappdel: '', bomreqd: '', constype: '', serialno: '',superfloc: '',edccode: '',maintplant: '',startupdate: ''}
];
const ELEMENT_DATA1: PeriodicElement1[] = [
  {first:'',second:'',third:'',fourth:'',fifth:''}
];
/**
 * @title Tree with dynamic data
 */
@Component({
  selector: 'app-floc-screen',
  templateUrl: './floc-screen.component.html',
  styleUrls: ['./floc-screen.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  //  encapsulation: ViewEncapsulation.None,
})
 

export class FlocScreenComponent {
  multiSelector;

  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  serializedDate = new FormControl();
  displayedColumns4: string[] = ['tabledatashow','objtype', 'pmassembly', 'make', 'model', 'shortext', 'action'];
  dataSource4 = ELEMENT_DATA4;
  displayedColumns5: string[] = ['tabledatashow','pmassembly', 'component', 'componentdescription', 'olto', 'qty', 'parent', 'seqid', 'action'];
  dataSource5 = ELEMENT_DATA5;
  displayedColumns6: string[] = ['tabledatashow','pmassembly', 'pmassemblydescription', 'floc', 'flocdescription', 'action'];
  dataSource6 = ELEMENT_DATA6;
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  loadinController:Boolean = false 
  flocLevel:any;
  floc1:any;
  searchResultSuccess:boolean=false;
  // objType;
  shortTextTable;
  perfStd;
  perfStdAppDeal;
  bomReqd;
  selectedTabIndex = 0;
  consType;
  serialNo;
  firstTableData;
  secondTableData;
  dropDownValues;
  docsRelatedData;
  @ViewChild('tree') tree;
  charactersWithDisabled: Array<IOption> = [
    {value: '0', label: 'Aech'},
    {value: '1', label: 'Art3mis', disabled: true},
    {value: '2', label: 'Daito'},
    {value: '3', label: 'Parzival'},
    {value: '4', label: 'Shoto', disabled: true}
];
  constructor(private cd: ChangeDetectorRef,public database: DynamicDatabase,private database2 :DynamicDatabase2,private basicService: BasicService, private router: Router, public dialog: MatDialog,public httpstatus:HTTPStatus ) {
    
    this.multiSelector = new FormGroup({
      activityId :new FormControl()
    })
    this.httpstatus.getHttpStatus().subscribe((data)=>{
      // console.log(data,'loggingdatas')

      this.loadinController = data
      this.cd.markForCheck();
    })
    // calling initial function
    this.initialCall();



                       
            


  }
  OnInit(){
     
  }
  toggleSelector(event){
    this.firstTableData.ExInd = event.checked
  }
  multiSelectorPerfStD:Array<any>=[];
  dataSource2: DynamicDataSource;
  documentList:Array<any> = []
  initialCall(){
      if(sessionStorage.getItem('access_token') && sessionStorage.getItem('refresh_token') && sessionStorage.getItem('refresh_token')!=undefined && sessionStorage.getItem('access_token')!=undefined){
    
      // get data from API for Tree content
      this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
      this.dataSource = new DynamicDataSource(this.treeControl, this.database, this.basicService);

      this.dataSource.data = this.database.initialData();
      // api call for the dropdown content
      this.basicService.listFlocBasicData().then((data:any)=>{
        if(data && data.message =='Success'){
          this.documentList = data.result[0].documentList
          console.log('insideini========>',this.documentList)
          this.getTreeData();
        }
        this.dropDownValues=data;
        // this.dropDownValues.result[0].perfStd.forEach((obj,index)=>{
        //   this.multiSelectorPerfStD.push({value:index,label:obj.PerfStd1})
        // })
          console.log(this.multiSelectorPerfStD,'aaaja')
        });

      //to detect scroll event
      window.addEventListener('scroll', this.scroll, true); //third paramete


 
}
else{
    this.router.navigate(['login']);
    }
  }
  nodeData;
  mouseHovers(flocPassed){
    console.log("eventCatched",flocPassed);
    this.basicService.treeViewFlocData(flocPassed).then((dataRes:any)=>{
      this.nodeData='';
      if(dataRes && dataRes.result[0] && dataRes.result[0].ShortText && dataRes.result[0].SuperiorFloc){
        if(dataRes.result[0].ShortText == null)
        {
          dataRes.result[0].ShortText = 'Not Available'
        }
        else if(dataRes.result[0].SuperiorFloc == null){
          dataRes.result[0].SuperiorFloc = 'Not Available'
        }
        else if(dataRes.result[0].ShortText != null && dataRes.result[0].SuperiorFloc != null ){
          this.nodeData = 'ShortText :'+dataRes.result[0].ShortText +', SuperiorFloc :'+dataRes.result[0].SuperiorFloc;
        }
        else{
          this.nodeData = 'No data found for floc'
        }

      }
    })
  }
  changeAdditionalData(selectedFloc){
    console.log('eventPassed',selectedFloc,this.documentList);

    let selectionChange = this.documentList.find(o=>o.SourceID == selectedFloc)
    this.docsRelatedData.DocID = selectionChange.DocID;
    this.docsRelatedData.DocDesc = selectionChange.DocDesc;

  }
  mouseHoverOnContextMenu(eventPassed){
    
  }
  countForRightClick:number=0;
  @HostListener('document:contextmenu', ['$event']) 
  mouseRightClickEvent = (event): void =>{
    console.log("eventmouse",event,event.target.className);
    this.countForRightClick++;
    if(this.countForRightClick%2==0){
    }
    else if(event.target.screenX < 499){
      // this.mouseHover(event.target.innerText.split(' ')[1],this);
      this.contextMenu.closeMenu();
      event.preventDefault(); // Suppress the browser's context menu   
    }
    else{
      console.log('right click on right split area');
    }
  }

  @HostListener('document:scroll', ['$event']) 
  scroll = (event): void => {
    if(this.objSearchedData.flocHint.length>3){  
      let pos = event.target.scrollTop + event.target.offsetHeight;
      let max = event.target.scrollHeight
      console.log("scrollll",pos,max);
          if (parseInt(pos) == max) {
            this.count+=50;
            this.objSearchedData.startLimit=this.count;
            this.basicService.searchFloc(this.objSearchedData).then((data:any)=>
            { 
              if( data.result != null && data.statusCode != 400){
                  this.dataForSearchTable.push(...data.result);
              }
              else{
                  this.basicService.openSnackBar('Error in Response','Backend Error');
                  }
            })
        }
        else{
            console.log("scroll is not at bottom");    
          }
    }
    else{
      // console.log("serach text length is less than 3 letter");
      
    }
}


  openDialog(): void {
    const dialogRef = this.dialog.open(FlocbottomdocumentmodalComponent, {
      data: {},
      panelClass: 'floc-bottom-table-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  description;
  descriptionBottom;
  // for table data
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'ex', 'abc', 'perfstd', 'perfstdappdel', 'bomreqd', 'constype', 'serialno', 'superfloc', 'edccode', 'maintplant', 'startupdate'];
  dataSource1 = ELEMENT_DATA;

  // table 2
  displayedColumns1: string[] = ['first', 'second', 'third', 'fourth', 'fifth'];
  myDataArray = ELEMENT_DATA1;

  maintplant: dropdata[] = [
    {id:1, value: '', viewValue: 'AU04' },
    { id:2,value: '', viewValue: 'AU05' }
  ];
 
  
  objTypeArray = [
    {id:0,value:'BOXJ'},
    {id:1,value:'VASO'},
    {id:2,value:'SWLI'},
    {id:3,value:'DESA'}
  ];
  perfStdArray=[
    {id:0,value:'F27'},
    {id:1,value:'P28'},
    {id:2,value:'F28'},
    {id:3,value:'P29'}
  ]
  // end of table data
  
  


  // mat menu for the context menu 
  myControl = new FormControl();
  
  options: string[] = [];


  getDataForContextMenu={'ShortText':'','SuperiorFloc':''};
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  onContextMenu(event: MouseEvent,flocPassed) {
    console.log("funcu",flocPassed,"<floc passed ====event>",event);
    this.basicService.treeViewFlocData(flocPassed).then((dataResponse:any)=>{
      if(dataResponse){
        if(dataResponse && dataResponse.result[0] && dataResponse.result[0].ShortText){
          console.log('====>',dataResponse.result[0], dataResponse.result[0].SuperiorFloc,dataResponse.result[0].ShortText);
        this.getDataForContextMenu.ShortText = dataResponse.result[0].ShortText;
        this.getDataForContextMenu.SuperiorFloc = dataResponse.result[0].SuperiorFloc;
        }
      }
      else{
        this.basicService.openSnackBar('Not fetch Data','Error');
      }
    })
    event.preventDefault(); // Suppress the browser's context menu
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.openMenu();
  }
  
  // for searching a Floc at right top panel
  searchFloc;
  flocSearched=[];
  previousSearchedFlocName;//to get the value next time when we come on search
  count=0;
  objSearchedData:any={'flocHint':'','startLimit':''};
  flocSearchedShow:boolean = false;
  matOption;
  dataForSearchTable:any;
  showSearchingLoader:boolean =false;
  serachingFlocMethod(event,flocHint){ 
    this.flocSearched = [];
    // console.log("searching Floc method11",event.target.value);
    this.objSearchedData.flocHint = event.target.value;

    if(event.target.value.length>3){  
      // console.log("searching Floc method2");
      this.showSearchingLoader = true;
      this.showSearchOptions=false;
      this.objSearchedData.startLimit = this.count;
      this.basicService.searchFloc(this.objSearchedData).then((data:any)=>
      { 
        
        this.showSearchingLoader = false;
        this.cd.markForCheck();
        console.log('this.showSearchingLoader',        this.showSearchingLoader);
        if(data.result &&  data.result.length > 0 && data.statusCode != 400){
          this.dataForSearchTable = data.result;
          this.showSearchOptions=false;
        }
        else if(data.result == null && data.statusCode === 400 ){
          this.matOption = "No Similar Data Searched";
          this.showSearchOptions=true;
        }
        else{
          this.matOption = "Error in response";
        }
      
      })
    }
    else if(event.target.value.length <= 3 && event.target.value.length > 0){
      this.matOption = ["Atleast 4 letters are minimum to search data"];
      this.showSearchOptions = true;
    }
    else if(event.target.value.length == 0){
      this.showSearchOptions = false;
      this.dataForSearchTable = 0;
    }
    else{
      console.log("else",);
    }
  }
 // end of for searching a Floc at right top panel


//  starting of -close button on search panel
showSearchOptions:boolean = false;
closeSearchBarOptions(){
  this.showSearchOptions=false;
}
//  end of -close button on search panel



  treeControl: FlatTreeControl<DynamicFlatNode>;
  dataSource: DynamicDataSource;

  getLevel = (node: DynamicFlatNode) => node.level;

  isExpandable = (node: DynamicFlatNode) => node.expandable;

  hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;

  // method to load the flocs at page load
  loading:boolean = false;
  getTreeData() {
    this.loading =true;
    this.basicService.treeViewData("").subscribe((data: any) => {
      console.log('thisss');
      this.loading =false;
      if (data.statusCode === 200) {
        if (data.result.length > 0) {
        
          this.database.dataMap = data.result[0].subNode;
          console.log('Tree1apiData',data.result[0].subNode);
          
          let root = data.result;
          console.log('tree1data.resultRootInitialise',data.result);
          
          root[0].subNode = [];
          this.database.rootLevelNodes = root;
          console.log('tree1Database',this.database);
          
          this.dataSource.data = this.database.initialData();
          console.log('tree1DataSource',this.dataSource.data);
          this.tree.treeControl.expandAll();
        }
      }
    });
  }

  // end
subNodes=[]
async getFlatedSubNodes(subnodes){
  if (subnodes.length == 0){
    for await (let node of this.subNodes){
      node.subNode = []
    }
         return this.subNodes
  }
   if (subnodes[0] && subnodes[0].subNode.length >= 0){
    this.subNodes.push(...subnodes)
     return this.getFlatedSubNodes(subnodes[0].subNode)
 
   }

   
}
  saveEXData()  {
let exData = {};
exData = this.secondTableData
this.basicService.updateExData(exData).subscribe((data)=>{
  console.log(data,'sadcscs')
  if(data['statusCode'] === 200){
    this.basicService.openSnackBar("Update Sucessfull",'Sucess')
  }else{
    this.basicService.openSnackBar("Error While Updating ",'Sucess')
  }
})
    }

 flocDataUpdateAfterSearch;
 forViewIconFlocMatch;
 async eventOnClick(flocPassed?,fromSearch?){
   this.forViewIconFlocMatch = flocPassed;
   if (flocPassed == undefined){
     flocPassed  = 'Project'
   }

    if(fromSearch == true || flocPassed  == 'Project'){
    this.basicService.searchExploreNodeData(flocPassed).then(async (dataResponse:any)=>{
      console.log(dataResponse,'aflated')
      if(dataResponse && dataResponse.result && dataResponse.result['0'] && dataResponse.result['0'].subNode){

      }
        let flatesdArray = dataResponse.result["0"].subNode
        this.subNodes = [];
         this.getFlatedSubNodes( dataResponse.result).then(async(data)=>{
          let len = data.length -1

          console.log(data,'xasxa')
          dataResponse.result[0].subNode = data;

          console.log(dataResponse.result[0].subNode,'flated')
          let root = dataResponse.result
          console.log(root,'resultsss')
          root[0].subNode = [];
          this.database.rootLevelNodes = root;
          console.log(this.database.rootLevelNodes,'flated')
         this.dataSource.data = await this.database.initialData();
         console.log(this.database,'dataMap')
          if(flocPassed != 'Project'){
            
            for (let i=0;i<len;i++){
             this.tree.treeControl.expandAll()
   
             }
          }else{
            this.tree.treeControl.expandAll()
          }
          


        })

      this.database.dataMap = dataResponse.result[0].subNode;
        console.log(this.database,'dataMap')

      console.log('dataResponse',this.treeControl, this.treeControl.dataNodes,dataResponse);
    
    
    })
  }
    this.flocSearchedShow = false; //for hiding the search bar suggestion on click event of eventClick method 
    this.flocSearched.length = 0; //for empty an array
    
    this.basicService.treeViewFlocData(flocPassed).then((data:any)=>
    {
      if(data.result[1] != null){

        this.secondTableData = data.result[1];
        (this.secondTableData.ProtectionConcept && this.secondTableData.ProtectionConcept.length>0)?this.secondTableData.ProtectionConcept =   this.secondTableData.ProtectionConcept.split(','):''
        Object.keys(this.secondTableData).forEach((obj)=>{
          if (this.secondTableData[obj] == null){
            this.secondTableData[obj] = ''
          }
        })
      }
      if(data.result[0] != null){
      this.firstTableData = data.result[0];
      Object.keys(this.firstTableData).forEach((obj)=>{
        if (this.firstTableData[obj] == null){
          this.firstTableData[obj] = ''
        }
      })
    }
    console.log('dataaaaa',this.firstTableData);

      if(data.result[2] != null){

        this.docsRelatedData = data.result[2];
        Object.keys(this.docsRelatedData).forEach((obj)=>{
          if (this.docsRelatedData[obj] == null){
            this.docsRelatedData[obj] = ''
          }
        })
      }

      console.log(this.firstTableData.StartUpDate,'date',this.firstTableData.StartUpDate.length)
      if(this.firstTableData && this.firstTableData.PerfStd){
        let letHelpInSplitting:any= this.firstTableData.PerfStd.split(";");
        this.firstTableData.PerfStd = letHelpInSplitting;
      }
      if(this.firstTableData.StartUpDate.length > 6 ){
        let dateArray =    this.firstTableData.StartUpDate.split('.') 
        if(dateArray.length ==  3)  this.firstTableData.StartUpDate=  new Date(dateArray[1]+"-"+dateArray[0]+"-"+dateArray[2])
        console.log(this.firstTableData.StartUpDate,this.serializedDate,dateArray,dateArray[0]+"-"+dateArray[1]+"-"+dateArray[2],'startupDAte=============================')
      }
    })
  }

  updateOtherFloc(){
    this.firstTableData['Floc'] = this.firstTableData['Floc1']   
    delete this.firstTableData.PerfStd ;
    this.basicService.updateOtherFlocData(this.firstTableData).subscribe((data)=>{
      console.log(data,'cascsc')
      if(data['statusCode'] === 200){
        this.basicService.openSnackBar('Update Successfull','Sucess')
      }else{
        this.basicService.openSnackBar('Error while updating data','Error')
      }

    })
  }

  

  // method used inside mouseHover method for floc to passed in API
  // flocFromEvent(flocBinded){
  //   console.log();
  //   return new Promise((resolve,reject)=>{
  //     if(flocBinded.search('chevron_right') == 0){
  //       this.flocToPassed=flocBinded.split('chevron_right')[1];
  //       console.log('hover@1');
  //       resolve();
  //     }
  //     else if(flocBinded.search('expand_more') == 0){
  //       this.flocToPassed=flocBinded.split('expand_more')[1];
  //       console.log('hover@2',flocBinded,flocBinded.split('expand_more')[1]);
  //       resolve();
  //     }
  //     else
  //     {
  //       this.flocToPassed = flocBinded;
  //     }
  //   })
  // }

// method for printing Xls format data
//  data= "grant_type=refresh_token&refresh_token="+sessionStorage.getItem('refresh_token');

//  for exporting excel sheet
public exportAsExcelFile(json: any[], excelFileName: string): void {
  const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
  const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
  const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
  this.saveAsExcelFile(excelBuffer, excelFileName);
}
private saveAsExcelFile(buffer: any, fileName: string): void {
   const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
   FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
}


// method call for download xls file
printDataInXls(){
  console.log(this.objSearchedData.flocHint.length,'flock hint')
  if(this.objSearchedData.flocHint.length > 3){
    this.basicService.exportData(this.objSearchedData.flocHint).then((data:any)=>{
      // console.log(data,'loggingdata')
      if(data){
        let xlsData = data.result;
        this.exportAsExcelFile(xlsData, 'searchDataXlsSheet');
      }
      else{
        this.basicService.openSnackBar('No data related to search','Try Again');
      }

    })
  }
  else{
    this.basicService.openSnackBar('please enter atleast 4 character for sheet in search bar','');
  }
}


// for uploading data using xls file
uploadedFileName
fileObj:File;
async uploadDataInXls(event){
  var reader = new FileReader();
  reader.readAsBinaryString(event.file);
  reader.onload = (e: any) => {
    /* read workbook */
    const bstr: string = e.target.result;
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
    /* grab first sheet */
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
    /* save data */
    var data = <any>(XLSX.utils.sheet_to_json(ws));
  
    // calling service for storing json data to their db which we get from csv or xls file from user
    this.basicService.saveFlocDataFromCsv(data).then((dataRes:any)=>{
    if(dataRes.statusCode == 200 && dataRes.message =="Success"){
      this.basicService.openSnackBar(event.file.name+' uploaded','');
    }
    else if(dataRes.statusCode == 400){
      this.basicService.openSnackBar('There is something mismatch with file','Error');
    }
    else{
      this.basicService.openSnackBar('Internal Server Error','Error');
    }
    });
  }
}

  // collapse the tree
  collapseAll(){
    this.treeControl.collapseAll();
  }
  convertDate(date:Date){
  let month =  date.getMonth() + 1;
  let parsedDate = date.getDate();
  let year = date.getFullYear();
   return [parsedDate,month,year].join('.')
  
  }
  saveExData(){

  }

  // method save data of right top panel  
  sendObj={'FlocLevel':'','Floc1':'','ObjType':'','ShortText':'','ExInd':'','ABC':'','PerfStd':'','PerfStdAppDel':'','BomReqd':'','ConsType':'','SerialNo':'','SuperiorFloc':'','EDCCode':'','MaintPlant':'','StartUpDate':''};
  saveTopRightData(){
    console.log(this.firstTableData,'csdssscscs')
    if(this.firstTableData && this.firstTableData.FlocLevel)
    {
      console.log('sendObj',this.sendObj);
      this.sendObj['FlocLevel'] = this.firstTableData.FlocLevel == ''?'null':this.firstTableData.FlocLevel;
      this.sendObj['Floc'] =this.firstTableData.Floc1 == ''?'null':this.firstTableData.Floc1;
      this.sendObj['ObjType'] =this.firstTableData.ObjType == ''?'null':this.firstTableData.ObjType;
      this.sendObj['ShortText'] =this.firstTableData.ShortText == ''?'null':this.firstTableData.ShortText;
      this.sendObj['ExInd'] =this.firstTableData.ExInd 
      this.sendObj['ABC'] =this.firstTableData.ABC == ''?'null':this.firstTableData.ABC;
      if(this.firstTableData.PerfStd.length>0){
      let perfStdChange = this.firstTableData.PerfStd.join(';');
      this.sendObj['PerfStd'] = perfStdChange == ''?'null':perfStdChange;
    }
      this.sendObj.PerfStdAppDel =this.firstTableData.PerfStdAppDel== ''?'null':this.firstTableData.PerfStdAppDel;
      this.sendObj.BomReqd =this.firstTableData.BomReqd== ''?'null':this.firstTableData.BomReqd;
      this.sendObj.ConsType =this.firstTableData.ConsType == ''?'null':this.firstTableData.ConsType;
      this.sendObj.SerialNo =this.firstTableData.SerialNo== ''?'null':this.firstTableData.SerialNo;
      this.sendObj.SuperiorFloc =this.firstTableData.SuperiorFloc== ''?'null':this.firstTableData.SuperiorFloc;
      this.sendObj['EDCCode'] =this.firstTableData.EDCCode == ''?'null':this.firstTableData.EDCCode;
      this.sendObj.MaintPlant =this.firstTableData.MaintPlant== ''?'null':this.firstTableData.MaintPlant;
      this.sendObj.StartUpDate =this.firstTableData.StartUpDate== ''?'null':this.firstTableData.StartUpDate;
      console.log('send1',this.sendObj.StartUpDate)
      if(this.sendObj && this.sendObj.StartUpDate != null){
        this.sendObj.StartUpDate = this.convertDate(new Date (this.firstTableData.StartUpDate))
      }

     console.log(this.sendObj,'send2')
     if(this.sendObj.StartUpDate === "NaN/NaN/NaN"){
        this.sendObj.StartUpDate = null;
       }
      this.basicService.saveFlocTopData(this.sendObj).then((dataRes:any)=>{
          if(dataRes.statusCode === 200 && dataRes.message === 'Success' ){
            this.basicService.openSnackBar('Data Successfully saved','Sucess');
            // call to get Tree Data After saving successfully the data of Floc
         //   this.removeTopFLocRow();
            this.getTreeData();
          }
          else{
            this.basicService.openSnackBar('Try again','Unsuccessful');
          }
      })
    }
    else{
      this.basicService.openSnackBar('Fields Must have data to save','Try again');
    }

  }
  selectedTab(event){
    this.selectedTabIndex=event.index;
    console.log(event,this.selectedTabIndex,'tabSWitched')
  }
  updateFlocBottomData(){
    if(this.selectedTabIndex == 0){
     console.log( this.docsRelatedData )
     let flockData = {}
     flockData['Floc'] = this.docsRelatedData.Floc
     flockData['DocID'] = this.docsRelatedData.DocID
     flockData['SourceID'] = this.docsRelatedData.SourceID
     flockData['DocDesc'] = this.docsRelatedData.DocDesc
     console.log('flockData');
     this.basicService.updateDocumentData(flockData).then((res)=>{
       console.log(res,'responseUpdateFloc')
       this.basicService.openSnackBar("Flock updated",'sucess')
     })
    }
    else if(this.selectedTabIndex == 1){
      this.saveEXData()
    }
    else if(this.selectedTabIndex == 2){
      this.updateOtherFloc();
    }
  }

  cssCount:number=0;
  show:boolean=false;
  showPmAssembly(eventPassed){
    console.log('eventPasssssssss',eventPassed);
    this.show =! this.show; 
  }

  rowAddCount:number = 0;
  addTopFLocRow(){
    this.rowAddCount++;
    if(this.rowAddCount == 1){
      if(this.firstTableData!=null){
        this.basicService.openSnackBar('New Row Added ','Success');
      }
      else{
        this.basicService.openSnackBar('Row Added','Success');      
        }
      this.firstTableData={
        "FlocLevel":"",
        "Floc1":'',
        "ObjType":'',
        'ShortText':'',
        "ExInd":false,//it take boolean value and not showing to user so static bind there to send in API
        "ABC":'',
        "PerfStd":'',
        "PerfStdAppDel":'',
        "BomReqd":'',
        "ConsType":'',
        "SerialNo":'',
        "SuperiorFloc":'',//superior FLoc is needed to save the data.Static bind data with it. 
        "EDCCode":'',
        "MaintPlant":'',
        "StartUpDate":'',
        "key":'addedRow'
      }

    }
    else{
      this.basicService.openSnackBar('Row Already Added','Success');
    }
  }

  removeTopFLocRow(){
    this.rowAddCount = 0
    if(this.firstTableData && this.firstTableData.key === "addedRow"){
      delete this.firstTableData;
      this.basicService.openSnackBar('Row Removed','Success');
    }
    else{
      // console.log();
      this.basicService.openSnackBar('No Row to Remove','Try again');
    }
  }


  changePro(e){
    console.log('aaaaaaaaa',this.secondTableData.ProConAppDel,e);
  }

  flocDelete(){
    console.log('flocss',this.firstTableData,this.firstTableData.Floc);
    if(this.firstTableData && this.firstTableData.Floc1 !=null && this){
      this.basicService.deleteFlocScreenData(this.firstTableData.Floc1).then((res)=>{
        if(res && res['statusCode'] === 200){
          this.getTreeData();
          this.firstTableData = [];
          this.basicService.openSnackBar('Successfully Deleted',false);
        }
        else{
          this.basicService.openSnackBar('Not able to delete','try again ');
        }
      })
    } 
    else{
      this.basicService.openSnackBar('Must Have Floc To delete',false);
    }
  }
}


import { BrowserModule } from '@angular/platform-browser';
import { NgModule,ChangeDetectorRef } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material-module';
import { AngularSplitModule } from 'angular-split';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// imports

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { BasicService } from '../app/services/basicService'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { MatMenuTrigger, MatDialogRef, MAT_DIALOG_DATA, MAT_DATE_LOCALE } from '@angular/material';
// import material services

import { DynamicDatabase } from '../app/floc-screen/floc-screen.component';
import { DynamicDatabase2} from '../app/floc-screen/floc-screen.component';
import { FlocbottomdocumentmodalComponent } from './flocbottomdocumentmodal/flocbottomdocumentmodal.component';
import { CdkTreeNode } from '@angular/cdk/tree';
import { MyHttpInterceptor,HTTPStatus } from './services/http-interceptor';
// import { MyHttpInterceptor } from './services/http-interceptor';
// angular material imports
// extra import for scrolling events

// for uploading a excel file
import { Ng2FileInputModule } from 'ng2-file-input';
import { RibbonComponent } from './ribbon/ribbon.component';
import { PmAssemblyComponent } from './pm-assembly/pm-assembly.component';
import { FlocScreenComponent } from './floc-screen/floc-screen.component';
import { OtherdatafirstmodalComponent } from './otherdatafirstmodal/otherdatafirstmodal.component';
import { OtherdatasecondmodalComponent } from './otherdatasecondmodal/otherdatasecondmodal.component';
import { PmsubassembliescomponentmodalComponent } from './pmsubassembliescomponentmodal/pmsubassembliescomponentmodal.component';


// 


@NgModule({
  entryComponents: [FlocScreenComponent,FlocbottomdocumentmodalComponent,OtherdatafirstmodalComponent,OtherdatasecondmodalComponent, PmsubassembliescomponentmodalComponent],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    FlocbottomdocumentmodalComponent,
    RibbonComponent,
    PmAssemblyComponent,
    FlocScreenComponent,
    OtherdatafirstmodalComponent,
    OtherdatasecondmodalComponent,
    PmsubassembliescomponentmodalComponent
  ],
  imports: [
    BrowserModule,
    AngularSplitModule.forRoot(),
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    Ng2FileInputModule.forRoot() // <-- include it in your app module
  ],
  providers: [BasicService,DynamicDatabase,DynamicDatabase2,  MatMenuTrigger,CdkTreeNode, RibbonComponent,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},


    HTTPStatus,
    // to import the Interceptor service
    {
    provide: HTTP_INTERCEPTORS,
    useClass: MyHttpInterceptor,
    multi: true //to include the multiple interceptor class in a project
  }
],
  bootstrap: [AppComponent],
  exports:[]
})
export class AppModule { }

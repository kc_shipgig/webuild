import { ContentChildren,Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PmAssemblyComponent } from '../pm-assembly/pm-assembly.component';

@Component({
  selector: 'app-otherdatafirstmodal',
  templateUrl: './otherdatafirstmodal.component.html',
  styleUrls: ['./otherdatafirstmodal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class OtherdatafirstmodalComponent implements OnInit {
  plantsData;
  plantValue;
  Rev;
  RefNo;
  selected;
  constructor(  @Inject(MAT_DIALOG_DATA) public  passedData,  public dialogRef: MatDialogRef<PmAssemblyComponent>){ 
    console.log('dialog',this.passedData);
    dialogRef.disableClose = true;
    (this.passedData['Rev'] == null)?this.passedData['Rev'] = '':''; 
    (this.passedData['RefNo'] == null)?this.passedData['RefNo'] = '':'';
    this.plantValue = this.passedData['Plant']
    this.plantsData = this.passedData['plant']
    }
    onNoClick = () =>{
      let obj = {
        "RefNo": '',
        "Rev": '',
        "Plant":''
      }
      
      this.passedData['RefNo'].length == 0 ? obj['RefNo'] = null:obj['RefNo']=this.passedData['RefNo'] ;
      this.plantValue.length == 0 ? obj['Plant'] = null:obj['Plant']=  this.plantValue;
      this.passedData['Rev'].length == 0 ? obj['Rev'] = null:obj['Rev'] =  this.passedData['Rev'] ;
      this.dialogRef.close(obj);
      console.log("inssssssssssssssss==================" , obj)
    }
  ngOnInit() {
  }

}

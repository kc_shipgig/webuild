import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherdatafirstmodalComponent } from './otherdatafirstmodal.component';

describe('OtherdatafirstmodalComponent', () => {
  let component: OtherdatafirstmodalComponent;
  let fixture: ComponentFixture<OtherdatafirstmodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherdatafirstmodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherdatafirstmodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
